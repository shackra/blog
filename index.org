#+TITLE: Inicio

El /Blog de Shackra/ es un pequeño blog escrito por [[http://es.gravatar.com/shackra][Jorge Araya Navarro]] (Shackra Sislock). Acá hablo de diversos temas, desde Software Libre hasta Catolicismo y apologética, como se ve, soy una persona con un estilo de vida muy variado.

Esta de más decir, por supuesto, que todas las opiniones expuestas en éste blog son enteramente mías (y si su autor no soy yo, pero he realizado la traducción al español y aparece aquí entonces quiere decir que sí comparto la opinión del autor original) y no necesariamente refleja la opinión de alguien más :).

He decido mover mi blog a [[https://github.com/kelvinh/org-page][org-page]] porque le he encontrado gusto y sabor a [[http://orgmode.org/][org-mode]]. ¡Desde ahora puedo escribir, editar y publicar mis entradas de blog desde [[http://www.gnu.org/software/emacs/][GNU Emacs]]!. Muchas gracias a [[https://github.com/kelvinh][Kelvin Hu]] por este grandioso aporte.

Esta sera la apariencia de mi blog de momento, aun no sé que tanto soporte para temas personalizados existe en org-page, pero esto no es algo que me urja entonces se quedara así.

Las fuentes de este blog [[https://bitbucket.org/shackra/shackra.bitbucket.org][estan disponibles también en su respectivo repositorio de Bitbucket]].
