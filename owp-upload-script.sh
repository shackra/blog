#!/bin/bash

set -e

export_dir='{{export-dir}}'
history_dir='{{history-dir}}'
publish_dir='{{publish-dir}}'
partial_update='{{partial-update}}'

rclone sync "$export_dir" rscf:elblog.deshackra.com
