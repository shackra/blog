#+TITLE:       Bautismo de niños cuando uno de los dos padres no es Católico
#+AUTHOR:      Jorge Araya Navarro
#+EMAIL:       elcorreo@deshackra.com
#+DATE:        2014-12-18 jue
#+URI:         /otros-escritos/%y/%m/%d/bautismo
#+KEYWORDS:    Iglesia Católica, protestantismo
#+TAGS:        Iglesia Católica, protestantismo
#+LANGUAGE:    en
#+OPTIONS:     H:3 num:nil toc:nil \n:nil ::t |:t ^:nil -:nil f:t *:t <:t
#+DESCRIPTION: respuesta a una consulta que tenia sobre el bautismo de mi sobrina

#+BEGIN_HTML
<a href="https://www.flickr.com/photos/markjsebastian/487050723/" title="IMG_20045 por mark sebastian, en Flickr"><img src="https://farm1.staticflickr.com/177/487050723_6004637a5f_b.jpg" width="710" height="472" alt="IMG_20045"></a>
#+END_HTML

#+BEGIN_HTML
<pre><code>
Return-path: <secretarioteologo@ive08.org>
Envelope-to: elcorreo@deshackra.com
Delivery-date: Thu, 18 Dec 2014 16:49:09 +0000
Received: from mail-wi0-f182.google.com ([209.85.212.182]:46047)
	by shared.dohost.us with esmtps (TLSv1:RC4-SHA:128)
	(Exim 4.84)
	(envelope-from <secretarioteologo@ive08.org>)
	id 1Y1eG9-0004At-9V
	for elcorreo@deshackra.com; Thu, 18 Dec 2014 16:49:09 +0000
Received: by mail-wi0-f182.google.com with SMTP id h11so2476182wiw.9
        for <elcorreo@deshackra.com>; Thu, 18 Dec 2014 08:49:04 -0800 (PST)
X-Google-DKIM-Signature: v=1; a=rsa-sha256; c=relaxed/relaxed;
        d=1e100.net; s=20130820;
        h=x-gm-message-state:sender:from:to:references:in-reply-to:subject
         :date:message-id:mime-version:content-type:content-transfer-encoding
         :thread-index:content-language;
        bh=+jR4s0+KwAf7gebEwj0evjN2gsWDJP0/q3wyRKZTEN0=;
        b=nJxSULvc1CN3nx+v0wMX/G9me5hvMkRGewTq9NYcnkTOhrFSFpbZKN2ad6koYLti+J
         hwPLD8ltiQQIU8WhDa0JsoiHX/Tw8nfP4Xx7qDQwRbghr0xP7csaoOYAwBz/A6/Sn/B8
         N7OTxL4oH3oWKaPjCCbG5TJYXoA5esxPVXT24gWqscCzqZ1N2yFHokBYPWUVsSfme0Bm
         K7ZtE7FfU5m33UM/2JB9TgXpSFsoOOxWWupRJt1Oy1aSuOC6S1CgMLa9eVvaqygIu4rR
         2MIImWjrchs4Bd5eWAfWpQzzFIrT2ERyEOsFUB65KTrMOUZzC7VXC2Kiklwh46AtB2RO
         dSTw==
X-Gm-Message-State: ALoCoQnIftU+cU6H+EP5jCceTuJDhsNYB4jPq8nalUJcHJ77X4ocVDAywYGndQbdnwk3uY0aIlj9
X-Received: by 10.180.87.36 with SMTP id u4mr25852753wiz.20.1418921344636;
        Thu, 18 Dec 2014 08:49:04 -0800 (PST)
Received: from FrJoseDell1525 ([94.228.15.99])
        by mx.google.com with ESMTPSA id d5sm9557717wjb.34.2014.12.18.08.49.02
        (version=TLSv1 cipher=ECDHE-RSA-AES128-SHA bits=128/128);
        Thu, 18 Dec 2014 08:49:03 -0800 (PST)
Sender: =?UTF-8?Q?Te=C3=B3logo_Responde?= <secretarioteologo@ive08.org>
From: "Secretario Teologo" <secretarioteologo@ive.org>
To: "'Jorge Araya Navarro'" <elcorreo@deshackra.com>
References: <6623f34fd5fc65137d915103bb5c74d1@www.teologoresponde.org>
In-Reply-To: <6623f34fd5fc65137d915103bb5c74d1@www.teologoresponde.org>
Subject: RE: Desde TEOLOGORESPONDE
Date: Thu, 18 Dec 2014 17:48:58 +0100
Message-ID: <006001d01ae2$85d0d050$917270f0$@org>
MIME-Version: 1.0
Content-Type: text/plain;
	charset="utf-8"
Content-Transfer-Encoding: quoted-printable
X-Mailer: Microsoft Office Outlook 12.0
Thread-Index: Ac++l73CbadVJewlTWCpOFMh4DBkXhcSm1mg
Content-Language: en-us
X-Spam-Status: No, score=-0.0
X-Spam-Score: 0
X-Spam-Bar: /
X-Ham-Report: Spam detection software, running on the system "shared.dohost.us",
 has NOT identified this incoming email as spam.  The original
 message has been attached to this so you can view it or label
 similar future email.  If you have any questions, see
 root\@localhost for details.
</code></pre>
#+END_HTML

Estimado Jorge: 

Lamento la tardanza en contestar. Hemos tenido muchas consultas y otros problemas últimamente. 

Es preciso recordar la enseñanza y el mandato de la Iglesia, que así manda obrar en su *Código de Derecho Canónico*, Canon 868 § 1: «Para bautizar lícitamente a un niño se requiere:

1. que den su consentimiento los padres, o al menos uno de los dos, o quienes hagan legítimamente sus veces.
2. que haya esperanza fundada de que el niño va a ser educado en la religión católica; si falta por completo esa esperanza, debe diferirse el bautismo, según las disposiciones del derecho particular, haciendo saber la razón a sus padres». 

Es decir que mientras sea un niño, el bautismo depende exclusivamente de los padres y nadie puede intervenir ese derecho. Para usted le queda la obligación de insistir, prudentemente, y defender la doctrina cristiana. Para esto y para sus conversaciones con su cuñada y el padre de ésta, le mando este texto de Martín Zabala, que estimo le será de gran utilidad por la buena disposición de argumentos bíblicos que presenta:

* BAUTISMO DE NIÑOS Y OBJECIONES PROTESTANTES
** Los niños al nacer sí tienen pecado.
Esto es algo que está muy claro en la Biblia, incluso en las versiones usadas por los hermanos protestantes, en todas dice esta verdad: “Tú ves que malo soy de nacimiento, pecador me concibió mi madre” (Salmo 51,5-7).

Desde hace más de dos mil años esta frase del salmista refleja la convicción profunda de ser pecador desde antes de nacer, y si se es pecador entonces esa es una de las razones para bautizar a los niños, pues el bautismo sirve para quitar los pecados. ¿De dónde puede venir este pecado por el simple hecho de nacer?

La respuesta bíblica a esto la encontramos en la carta que San Pablo escribió a los romanos que dice: “Por tanto, como por un solo hombre entró el pecado en el mundo y por el pecado la muerte y así la muerte alcanzó a todos los hombres, por cuanto todos pecaron... En efecto, así como por la desobediencia de un solo hombre, todos fueron constituidos pecadores, así también por la obediencia de uno solo todos serán constituidos justos” (Rom 5,12.19).

Por esto, hemos creído siempre que la miseria que oprime a los hombres en su inclinación al mal y a la muerte, no son comprensibles sin su conexión con el pecado de Adán y con el hecho de que nos ha transmitido un pecado, de ahí que lo llamemos pecado original.
Eso significa la privación de la santidad y justicia originales. Entonces bíblicamente hay un “pecado” desde el nacimiento, por eso es importante y bueno el bautismo desde pequeño. Pero, veamos otras razones que la Biblia nos da para esto.

** Dejen que los niños vengan a mí.
Esta frase de Nuestro Señor Jesucristo es como un indicador sobre lo que estamos hablando. Textualmente Jesús dijo: “Dejen que los niños vengan a mí, y no se lo impidan porque de los que son como ellos es el Reino de los Cielos” (Mt 19,14).

Si Jesucristo quiere que los niños estén junto a él, ¿Cuál es la mejor forma de estar plenamente unido a él en este tiempo? El bautismo precisamente. Vayamos una vez más a la Palabra de Dios para conocer la respuesta a esto: “Porque en un solo Espíritu hemos sido todos bautizados, para formar un cuerpo” (1 Cor 12,13).

Según San Pablo, es mediante el bautismo que pasamos a formar parte del cuerpo de Cristo que es la Iglesia. Así, por esta segunda razón de obediencia a Jesús y amor a los niños, es que se les bautiza desde pequeños.

** El Bautismo es necesario para entrar al Reino.
Además de lo señalado anteriormente, Jesucristo nos habla del Bautismo como algo necesario para entrar al Reino de los cielos. Esto lo encontramos en el evangelio de Juan: “Respondió Jesús: ‘En verdad, en verdad te digo: el que no nazca de agua y de Espíritu no puede entrar en el Reino de Dios. Lo nacido de la carne, es carne; lo nacido del Espíritu, es espíritu’.” (Jn 3,5-6).

Al nacer de nuestros padres nacimos de la carne, pero una cosa necesaria para entrar el Reino es nacer del Espíritu y esto es precisamente otra de las razones para bautizar al niño. Recordemos que no es un catecismo donde lo leímos, sino Jesucristo mismo en la Palabra de Dios quien lo afirmó.

** La promesa es para sus hijos.
Cuando se trata de ver por qué creemos en una u otra verdad, es fundamental el conocer bien las Escrituras. Al conocerlas el segundo paso es creer lo que en ella encontramos y no tratar de encontrar lo que nosotros queremos ver.

Eso fue lo que una vez sucedió en un programa de televisión, donde por casualidad me topé con él. Era un pastor protestante dando un estudio bíblico y diciendo que él creía que el bautizar a los niños era algo fundamentado en la Sagrada Escritura. Él, aunque no era católico, había aceptado que era cierto, que la Biblia enseñaba el bautismo de los niños, porque encontró la siguiente evidencia de ello: “Pedro les contestó: ‘Conviértanse y que cada uno de ustedes se haga bautizar en el nombre de Jesucristo, para perdón de sus pecados; y recibirán el don del Espíritu Santo; pues la Promesa es para ustedes y para sus hijos’.” (Hech 2,38-39).

Era claro. Si la promesa del Espíritu Santo es también para “sus hijos” entonces por qué privarlos de ese gran regalo a través del bautismo. No había que buscarle otra interpretación que lo que nítidamente afirmaba. Por eso, aunque él era protestante, basándose en la Biblia, el camino a seguir era el dar ese sacramento a los niños porque “la promesa de recibir el Espíritu también es para sus hijos”.

** Basta la fe del padre para bautizar.
Estudiando un poco más la Palabra de Dios encontramos el caso del carcelero, al cual se le pidió la Fe para después bautizar a su familia.

“Le respondieron: ‘Ten fe en el Señor Jesús y te salvarás tú y tu casa’. Y le anunciaron la Palabra del Señor a él y a todos los de su casa. En aquella misma hora de la noche el carcelero los tomó consigo y les lavó las heridas; inmediatamente recibió el bautismo él y todos los de su casa” (Hech 16,31-33).

Algo similar a eso sucede cuando en la Iglesia católica se bautiza, pues la fe se le pide a los papás y padrinos para que el niño pueda recibir los beneficios espirituales del bautismo. En realidad esto de pedir la fe a otros no es algo extraño en la Biblia.
¿Acaso Lázaro tuvo fe cuando fue resucitado? (Jn 11); o ¿Qué fe miró en el ciego y mudo que sanó? (Mt 12,22).

En estos y muchos casos más, lo que miraba Jesús no era la fe del enfermo ni mucho menos del que había fallecido, era la fe de los que lo llevaban lo que él miraba. Ni la fe, ni el estar consciente fue un requisito para sanar al mudo y resucitar a Lázaro. Con mayor razón Jesús mirará la fe de los padres y padrinos al pedir el bautismo para el pequeño (Hech 16,31-33: Lc 11,11-13).

A quien piensa diferente podría preguntarle ¿Acaso cuando un niño está enfermo necesita estar consciente para que surta efecto la medicina? o ¿No vale la ciudadanía porque el niño no estaba consciente de lo que adquiría? ¡Claro que sirve la medicina y también la ciudadanía!

Si creen en eso ¿Cómo no creen en el poder de Dios para dar su Espíritu? Es cuestión de creer y obedecer a Jesucristo.
Que increíble. El protestante que tanto grita y dice tener fe, a la hora de la verdad cree más en la medicina y en las leyes que en el Poder de Dios para enviar su Espíritu.

** Jesús no recibió el Espíritu Santo en el Río, sino cuando salió. El agua solamente es un símbolo.
Así como lo está leyendo. Vamos a ver directamente la Biblia para confirmarlo: “Y sucedió que por aquellos días vino Jesús desde Nazaret de Galilea, y fue bautizado por Juan en el Jordán. En cuanto salió del agua vio que los cielos se rasgaban y que el Espíritu, en forma de paloma, bajaba a él” (Mc 1,8-9).

Así que bíblicamente el Espíritu Santo no lo recibió Jesús en el agua, sino fuera de ella. El agua solamente es un símbolo; es un error pensar que entre más agua hay más Espíritu. Es como un semáforo: Si está en verde quiere decir que sigamos adelante, no importa el tamaño del foco. No vaya a pensar alguien, como algunos protestantes en el bautismo, que si el foco verde está más grande quiere decir que vaya más aprisa. El agua es un símbolo del Espíritu solamente, no importa la cantidad.

Es por eso que no es obligatorio que sea en un río, sino que puede ser con un poco de agua como símbolo y nada más. De hecho, los Apóstoles fueron bautizados en el Espíritu Santo en Pentecostés y estaban en una casa donde obviamente no había río (Hech 2,1-4).

** Hay que hacer lo que Jesús enseñó, no lo que él Hizo.
Por último, hermano, cuando alguien le comente qué debe de ser de adulto como Jesús lo hizo, habría que decirle que si él hace como Jesús, entonces después de bautizarse tiene que ir al desierto y ayunar 40 días y 40 noches como Jesús, vestir como Jesús, ser circuncidado como Jesús y morir como Jesús.

Por eso no se trata de hacer todo lo que él hizo, sino lo que él nos enseñó a hacer.
La frase de que “hay que hacer todo como Jesús” es un viejo truco que cuando conviene lo usan y cuando no, lo hacen a un lado. Se trata más bien de vivir lo que Jesucristo nos enseñó, y por eso es que se bautiza a los niños, pues hay una base bíblica.

* Posdata
Hay personas que desconocen las escrituras y se hacen bautizar varias veces como si el bautismo anterior no sirviera y cada vez que cambian de secta religiosa se vuelven a bautizar. Hay que recordarles que San Pablo dijo “Hay UN SOLO Bautismo” (Ef 4,4-5).
Esperamos haber dado respuesta a su inquietud.

Para fortalecerte más te recomiendo el Testimonio de conversión de Fernando Casanova Ex-Pastor Pentecostal. Es un video impactante donde él explica las razones bíblicas por las que decidió hacerse Católico».

#+BEGIN_HTML
<iframe width="710" height="533" src="https://www.youtube.com/embed/iXiFq4b3Lyk?rel=0" frameborder="0" allowfullscreen></iframe>
#+END_HTML

Dios le bendiga

/P. Juan Manuel Rossi/, [[http://www.iveargentina.org/][/IVE]]/.
