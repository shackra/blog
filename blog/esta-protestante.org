#+TITLE:       Está protestante en los comentarios de un vídeo Católico en YouTube
#+AUTHOR:      Jorge Araya Navarro
#+EMAIL:       elcorreo@deshackra.com
#+DATE:        2014-11-11 mar
#+URI: /blog/%y/%m/%d/esta-protestante
#+KEYWORDS:    protestantismo, Iglesia Católica
#+TAGS:        protestantismo, Iglesia Católica
#+LANGUAGE:    en
#+OPTIONS:     H:3 num:nil toc:nil \n:nil ::t |:t ^:nil -:nil f:t *:t <:t
#+DESCRIPTION: Observaciones sobre un encuentro con una ferviente anti-católica Protestante

https://upload.wikimedia.org/wikipedia/commons/thumb/7/72/Dirck_van_Delen_-_Beeldenstorm_in_een_kerk.jpg/1024px-Dirck_van_Delen_-_Beeldenstorm_in_een_kerk.jpg

Una protestante, en la sección de comentarios de YouTube: /«sabias que muchas almas de las que se arrodillan ante la virgen ahora estan en el infierno?»/

Mira tu, una blasfemia y una herejía en la misma pregunta. Blasfemia porque la veneración a la Madre de Dios no puede ser causa de pecado grave, que es lo que separa al hombre de Dios condenándole así al infierno. Y herejía porque nadie conoce lo secreto en el corazón de los hombres más que Dios, como para decir que hay gente en el infierno o no por X razón.

Si tengo suerte, quizás me diga que ella esta segura de estar en estado de gracia (o salvada porque /acepto a Jesús en su corazón como su Señor y Salvador/ que para el caso es decir lo mismo)[fn:1], seguro así hace el trió: blasfemia, herejía, herejía.

Amigos protestantes que aun tienen sus cabezas sobre los hombros: paren y piensen cualquier cosa mala que le digan sobre la Iglesia, porque si no son verdad esas cosas y ustedes acaban /tragándose el cuento/ lo único que van a convertirse es en heraldos de falsedad, y les va ir peor si en realidad estaban equivocados sobre la Iglesia y resulta ser en verdad la Esposa de Cristo ¿Y exactamente quien se queda apacible cuando hablan mal de quien más uno ama? Difícilmente Cristo lo sera.

Cómo puede apreciar, es más inteligente predicar sobre lo que uno ama, a predicar sobre lo que uno odia.

[fn:1] La gracia, siendo de orden sobrenatural, escapa a nuestra experiencia y sólo puede ser conocida por la fe. Por tanto, no podemos fundarnos en nuestros sentimientos o nuestras obras para deducir de ellos que estamos justificados y salvados (Concilio de Trento: DS 1533-34). Sin embargo, según las palabras del Señor: “Por sus frutos los conoceréis” (Mt 7, 20), la consideración de los beneficios de Dios en nuestra vida y en la vida de los santos nos ofrece una garantía de que la gracia está actuando en nosotros y nos incita a una fe cada vez mayor y a una actitud de pobreza llena de confianza:

#+BEGIN_QUOTE
Una de las más bellas ilustraciones de esta actitud se encuentra en la respuesta de santa Juana de Arco a una pregunta capciosa de sus jueces eclesiásticos: «Interrogada si sabía que estaba en gracia de Dios, responde: “Si no lo estoy, que Dios me quiera poner en ella; si estoy, que Dios me quiera conservar en ella”» (Santa Juana de Arco, Dictum: Procès de condannation).
#+END_QUOTE

~Catecismo de la Iglesia Católica, párrafo 2005.
