#+TITLE: El ateo con ansias de responder mis seis preguntas
#+AUTHOR: Jorge Araya Navarro
#+EMAIL: elcorreo@deshackra.com
#+LANGUAGE: en
#+URI: /blog/%y/%m/%d/%t/
#+DESCRIPTION:
#+OPTIONS: H:3 num:nil toc:nil \n:nil ::t |:t ^:nil -:nil f:t *:t <:t
#+TAGS: Dios, debate, ateismo, Iglesia Católica
#+KEYWORDS: Dios, debate, ateismo, Iglesia Católica
#+DATE: 2013-03-16

#+BEGIN_QUOTE
  Esto lo publique anteriormente en la comunidad [[http://www.taringa.net/comunidades/catolicost/6900531/MP-El-ateo-con-ansias-de-responder-mis-seis-preguntas.html][Católicos en T!]].
#+END_QUOTE

Andaba posteando comentario cuestionablemente-caritativas en el debate que tuvo el Cardenal George Pell contra
Richard Dawkins, video disponible en YouTube (No puedo decir que sea uno de mis debates favoritos).

Luego apareció este muchacho que clamaba a viva voz que la Biblia estaba llena de contradicciones y que si
estudiamos detenidamente el universo, de forma objetiva y científica, encontraremos que no hay Dios.

No recuerdo qué le dije, pero lo que le dije lo hice con un tono más serio... Oh bueno, le hice 6 preguntas
que, según yo, no hay forma de responderlas siendo ateo, son paradójicas (o al menos las respuestas no están
alineadas con esa forma de ver el mundo). El muchachillo, pues, me las respondio y yo, al ver lo errado que
estaba, decidí devolverle una respuesta, Ahí les va:

Yo: veamos si te gustan las paradojas que llegaría a encontrar siendo ateo: (1)¿Por que existe algo en lugar
de nada? (2)¿Por qué existo? (3)¿Para qué existo? (4)¿Por qué la ciencia asume de fe que el "ser es
inteligible"? (5)¿Cual es la ontologia de la moralidad objetiva? (6)¿Por qué la más simple de las células es
tan﻿ extrañamente compleja si fue producto del azar? Me gustaría oír tus respuestas objetivas y sin juicios
"pre concebidos".

Él (en un mensaje privado): /Adoro estas sesiones de preguntas, porque muchas veces me revelan la perspectiva
antropológica de los creacionistas/ [¡¿Qué yo soy qué!?] /:), a ver:/

#+BEGIN_QUOTE
*1)* Para decirme porque existe algo en lugar de nada, primero tendrías que darme un ejemplo de "Nada" y ¿por
qué? porque nunca has tenido nada, nadie hasta ahora ha sido capaz de definir, medir, caracterizar o describir
"Nada", debes saber que nuestro universo siempre ha existido ya sea en distintos estados de materia, pero
siempre ha existido recuerda E=mc al cuadrado, eso indica que energía es igual a materia y la materia no se
crea ni se destruye, se transforma.
#+END_QUOTE

Yo (en un mensaje privado de respuesta): *R1)* Nuestro universo no ha existido siempre; Tiene alrededor de
unos 13.77 billones de años de edad y antes de la singularidad existía nada. Esta declaración es
religiosamente neutral, muy discutida en los círculos académicos y la puedes encontrar en cualquier libro de
cosmología moderna. Decir que el universo siempre a existido "en diferentes estados" es saltarse olímpicamente
la segunda ley de la termodinámica que establece que "La cantidad de entropía del universo tiende a
incrementarse en el tiempo", entre más entropía exista en el Universo, menos capacidad para hacer trabajo
tendrá, un universo sin energía es un universo muerto.

Diferentes modelos cosmológicos para eludir el inicio del universo han sido propuestos, sin embargo en 2003
tres cosmólogos lideres en su campo Arvin Borde, Alan Guth, y Alexander Vilenkin pudieron probar
que *cualquier* universo que se haya podido expandir, en promedio, con una velocidad mayor a 0, han de tener
un pasado finito. El principio de conservación de la energía si aplica, pero dentro de un universo creado. Si
el universo hubiera existido siempre, entonces deberíamos de ver una alta cantidad de entropía en el universo,
quizás una cantidad infinita de entropía ya que el pasado del universo es infinito como sugieres. Nada es
no-ser, Ser es lo contrario de Nada; un ejemplo de esto es cuando mueres, si el ateísmo es cierto, nosotros
seremos nada, dejaremos de ser.

#+BEGIN_QUOTE
*2)* Existimos porque somos el producto de la unión de dos gametos haploides lo cual nos hace seres
diplioides, que sepas que cada gametos tiene la mitad de la información para combinar gametos para la
siguiente generación, eso dio paso al origen del sexo, tus padres simplemente unieron sus gametos dándote
origen a ti, por eso existes y no porque una fuerza cósmica te creo en el vientre de tu madre, cada ser vivo
nace de uno anterior.
#+END_QUOTE

*R2)* Tu descripción biológica es correcta (no al menos como sugieres que el sexo surgió, aun es un
misterio para los biólogos cómo fue que el sexo apareció en los seres vivos siendo que su orígenes se remontan
a seres simples que se dividían). Yo existo porqué mi existencia fue acondicionada por mis padres, la
respuesta es correcta hasta que te das cuenta que la razón por la cual tus padres existen no esta en la propia
esencia de su ser, entonces para responder satisfactoriamente "por qué existo" debes explicar la existencia de
tus padres. Yo, tu, tus padres, tus abuelos y tus tatarabuelos (junto con el oxigeno, el alimento, el agua, el
planeta tierra y las estrellas, etc) son "seres contingenciales". Un ser contingencial es aquel ser que puede
o no existir, su existencia no es obligatoria. Un ser contingencial no posee en su esencia la explicación su
propia existencia, por ello se recurren a otras causas para explicarla. Si estas causas son también
contingenciales, debes apelar a las causas que acondicionaron la existencia de esas causas que acondicionaron
(y mantienen) tu existencia.

Cuando recurres a otras causas contingenciales de manera indefinida (ad infinitum) para tratar de explicar
por qué existes no estarías explicando nada en lo más mínimo, sino posponiendo la respuesta. Entonces, debemos
llegar a una realidad que no sea contingencial, que sea necesaria y que posee en su esencia la explicación de
su propia existencia para explicar la nuestra. Esta realidad nosotros los católicos la llamamos Dios. Habiendo
establecido lo anterior la respuesta a "¿Por qué existo?" no acaba cuando afirmas que vienes de tus padres.
Filosofía Tómista FTW!

#+BEGIN_QUOTE
*3)* El propósito de la vida desde la perspectiva creacionista es algo para reírme de verdad, es decir
existo porque dios me creó y debo seguir una serie de reglas y parámetros creados por él, si lo hago iré al
paraíso por toda la eternidad para vivir como un hincha de el, y si no sigo tales parámetros iré al infierno
donde seré castigado toda la eternidad, hey, pero te ama!, tu pregunta está totalmente equivocada y refleja el
gran miedo que sientes de asumir que dios no existe, y lo entiendo, yo una vez fui creyente como tú, y sentía
el mismo miedo y terror de solo pensar en la no existencia de dios, a muchos seres humanos les cuesta asumir
el hecho de que cuando mueres, ya, se acabo, eso es todo, no hay un alma que se dirija a X lugar para hacer X
cosa, y por eso muchas personas cuando van a morir (muchos creyentes entre ellos) se deprimen enormemente ,
ellos bien saben que es mentira, tu pregunta refleja la carencia de control en tu vida, ¿crees acaso que dios
nos traza el camino a todos nosotros desde que nacemos hasta morimos? ¿Y eso llena el propósito? No mi amigo,
eres tú, tú tienes el control de tu vida, el para que existimos es algo que irá en dependencia de nuestro
propio concepto de bienestar por tanto es imposible responderlo en un contexto objetivo, por una razón que te
diré más adelante.
#+END_QUOTE

Yo: *R3)* *No soy "creacionista". Creo que ya has notado que no lo soy.* /¿Conoces a Sísifo? En la mitología
Griega, Sísifo fue castigado por los dioses [al tratar de pasarse de listo con ellos]. Fue condenado a empujar
una gran roca por una colina empinada una y otra vez. Albert Camus al escribir el Mito de Sísifo nos habla de
la filosofía del absurdo, la existencia del hombre es finita y el valor que él le da a su vida es menor de lo
que él cree, sin embargo, hace tareas absurdas una y otra vez como si su existencia fuera útil. ¿Qué
diferencia existe entre el suicidio y una vida absurda? Ninguna. Lo que salva al hombre de su destino suicida,
según Camus, es la acumulación de vivencias.

Entonces, si el ateísmo esta en lo correcto, no existe un dios y no existe una alma, sencillamente pujamos
una gran roca en una ladera inclinada [Todos los dias, con el mero acto de despertar y prepararnos el
desayuno]. Aceptar lo anterior y no deprimirse es cegarce completamente con la fe de que nuestras actos valen
algo. La respuesta secular para la pregunta "¿Para qué existo?" se responde "No existes para nada más que
acumular experiencias y luego de un tiempo, morir."

Por supuesto, si el cristianismo esta en lo correcto y existe, no un dios, pero sí Dios, entonces la
respuesta es "tu existes para disfrutar en la eternidad del amor de Dios todopoderoso que te a creado". El
hecho de explicar el origen o la razón por la cual hago una pregunta no la invalida para nada, se invalida si
puedes refutar satisfactoriamente el cuestionamiento planteado. Si estamos de acuerdo con/ */R1/* /y/ */R2,/*
/podemos declarar que la respuesta cristiana a [para qué] existo es la más plausible./ Por supuesto, tu
conocimiento sobre el infierno, porque las almas se condenan y van allá es formidable,

#+BEGIN_HTML
  <iframe width="710" height="399" src="https://www.youtube.com/embed/x8zhnooySk4?rel=0" frameborder="0" allowfullscreen></iframe>
#+END_HTML

#+BEGIN_QUOTE
*4)* ¿Ciencia con fe? Amigo, te recuerdo que la ciencia es un método de observación científico y que esta
representa modelos que explican, el universo. El hombre se ha servido de las matemáticas y otros idiomas y
métodos para buscar datos que se puedan medir y corroborar. La fe se trata de aceptar cosas que van en contra
de las evidencias y datos. Fe y ciencia son incompatibles, así que mejor acomoda esa pregunta.
#+END_QUOTE

Yo: *R4)* La ciencia asume de fe que "el ser es inteligible", bajo este principio descansa la ciencia y es
innegable. ¿Puede ella demostrar con datos que se "pueda medir y corroborar" la idea de que el ser es
inteligible?. Cuando un científico va a investigar nuestra realidad finita que llamamos universo, hace de
forma implícita esta suposicion. Parece afirmar tácitamente que "aquello que no pueda ser comprobado por el
método científico no es verdad", dime entonces, ¿Esta científicamente comprobada la frase "aquello que no
pueda ser comprobado por el método científico no es verdad"?

Por ello me gustaría saber, desde una perspectiva secular, por que la ciencia hace una suposicion mística de
que el ser es inteligible [Si no puede ni comprobar la veracidad de la suposición de forma cientifica]. La fe
es confiar. No tiene nada que ver con carecer de datos "medibles y verificables", tener fe es un acto racional
que todo ser hace en base a evidencia que le ha sido presentada.

#+BEGIN_QUOTE
*5)* ¿Moralidad objetiva? A mi tampoco me gustó mucho la idea, pero tu pregunta vuelve a estar mal
planteada, y es por el simple hecho de que la moralidad es subjetiva, debemos abordar algo simple y es el
concepto de la moral que es: "son las reglas o normas por las que se rige la conducta o el comportamiento de
un ser humano en relación a la sociedad, a sí mismo o a todo lo que lo rodea", no todas nuestras sociedades
hoy en día tienen los mismos concepto de conducta amoral, te pongo el ejemplo del medio oriente donde una
mujer por no estar totalmente cubierta es lapidada o donde le aplican la ablación del clítoris a la mismas, a
ti y a mi nos parecerá monstruoso eso pero allí es donde chocamos con la subjetividad de la moral, esas son
las reglas de esa sociedad para alcanzar su bienestar, incluso tus parámetros morales también deben ser muy
contrarios a los míos, no estoy en contra de homosexualidad o del aborto, estoy más que seguro que tu si, y es
porque como dijo "Madeupstardust" la moral son como colores, cada quien tomara uno distinto para llegar al
bienestar, cada quien tomara su propio camino, por tanto la moral no puede ser objetiva.
#+END_QUOTE

Yo: *R5)* *Esa es la definición para la ética normativa. La Wikipedia en inglés tiene una definición más
exacta: "La moral (del latín /moralitas/ "manera, el carácter, la conducta apropiada"; es la diferenciación de
las intenciones, decisiones y acciones entre las que están "bien" (o correcta) y los que son "malas" (o
equivocada).

Ahora vamos a decir que la moral es subjetiva. Decir que la moral es subjetiva es decir que todas las reglas
morales son igualmente correctas, entonces, lapidar a una mujer por no usar el velo o aplicarle la ablación
del clítoris no nos debe de parecer monstruoso pero sí seriamos indiferentes hacia estos actos ¿Por qué? Por
que en la moralidad subjetiva, todas las reglas morales son correctas, ninguna esta equivocada. No importa que
estas mujeres puedan crear su propio concepto de moralidad para alcanzar "el bienestar".

Sin embargo, tener una sensación de monstruosidad hacia las personas que en medio oriente se le hace eso a
las mujeres atestigua para ti y para mi que algo no esta bien con sus reglas de moralidad, en la moralidad
subjetiva, pensar que una visión de la moral de otra cultura "le falla algo" es ir en contra de esta idea de
moralidad. Más simple:

Sí para mi no es moral que los homosexuales no deben contraer "matrimonio" o los niños ser abortados, puedes
pensar que algo no esta bien con mis ideas de moralidad ¿Pero con que derecho haces ese juicio si todas las
visiones de la moralidad son correctas? Una discusión similar sostuvo TheCartesianTheist y BionicDance en
Youtube: [[http://www.youtube.com/watch?v=Nn-ZggPYmvg][watch?v=Nn-ZggPYmvg]].

*Decir que la moral no puede ser objetiva es decir que no podemos pensar que algo anda mal con la moralidad
del prójimo. ¿Al menos practicas lo que predicas?*

/Creo que acá él ignoro que yo use el "si" condicional y no el "sí" afirmativo. en fin.../

#+BEGIN_QUOTE
*6)* ¿Acaso crees que un sistema tan complejo como el ser vivo depende de algo tan simple como el azar?
No mi amigo, afirmar que en la naturaleza las fuerzas son ciegas y que no hay dirección ni intención de crear
nada hace referencia a la absurda analogía de "Un huracán que crea un avión de guerra cuando sopla en una
chatarrera" querer hacer ver que los intentos al azar cuando son repetidos indefinidamente son capaces de
crear organismos muy complejos es totalmente deshonesto, si esas fuerzas fueran en realidad tan aleatorias y
lograran crear organismos tan complejos estas por lógica también deberían ser capaces de destruirlos, es como
que un hombre ciego levante un castillo de 1000 naipes accidentalmente y luego diga "Ya lo cree y no lo voy a
destruir" es absurdo, si el azar crea complejidad es obvio que también lo va a destruir, las especies cambian
bajo determinados procesos de modificación en el cual las especies se adaptan a su medio ambiente dependiendo
de qué tan efectiva es la especie en términos de supervivencia.

Yo no te voy a realizar preguntas, eres un muchacho inteligente por las veces que te he leído, y en vista de
eso solo debo sentarme a esperar que tu mismo te des cuenta, no espero cambiar tu mentalidad para nada, solo
te respondo tus preguntas porque me gusta defender mis argumentos, que tengas un buen día.

*PS:* Por cierto tus preguntas no fueron paradojas de ateos, fueron interrogantes donde debiste estar seguro
que no habrían respuestas, por eso usas el calificativo "Paradoja" que sepas que la misma es una idea extraña
opuesta a lo que se considera verdadero o a la opinión general, es una proposición en apariencia verdadera que
conlleva a una contradicción lógica o a una situación que infringe el sentido común, y eso amigo mio si que
sale en tu sagrado libro por todas partes ;)
#+END_QUOTE

Yo: *R6)* Por favor explícame ese salto de fe. ¿Cómo puede una serie de fuerzas mecánicas y no consientes
"crear" o poseer "intención"? Estas características son propias de seres con consciencia de sí mismos. Por qué
soy deshonesto al sugerir que los organismos complejos son así si vienen producto del azar (¿Estas seguro que
la moralidad es subjetiva? Si es así entonces no hay nada de malo en ser deshonesto). Yo hago la pregunta por
que *A)* Los organismos complejos existen (como muestra un botón: El cerebro humano es "producto de la
naturaleza", desarrollado por una serie de fuerzas no conscientes [según el ateísmo, claro] pero aun así,
formidablemente misterioso debido a su complejidad) y *B)* Dios, según tu, no existe. Por favor, explícame
como puede ocurrir algo tan paradójico como esto. ¿Puede ser que las fuerzas en la naturaleza tienen algo de
conciencia sobre sí mismas a pesar de ser mecánicas y haberse creado en el mismo momento en que se creo el
universo? Quizás si empiezo a saltar la gravedad se canse de mi y no me jale de nuevo a la tierra.

La existencia de una singularidad, de un universo, de nuestra existencia y complejidad son evidencia de que
Dios existe de la misma forma que La Mona Lisa da testimonio que un pintor llamado DaVinci existió alguna vez
(o en términos generales, una bella pintura da testimonio de un ser creativo y racional que la llevo acabo).

O quizás sea posible que en la inexistencia de vida en el universo y en un tiempo relativamente enorme,
digamos, billones de billones de años en el futuro, ¿Se formaría una computadora con su sistema operativo y
software por obra y gracia de las fuerzas de la naturaleza?

/Reflexiona al respecto./

*P.S:* La Iglesia Católica creo la ciencia moderna por aya en 1170 ;)
[[http://patheos.com/blogs/badcatholic/2012/04/religion-religioff.html][patheos.com/blogs/badcatholic/2012/04/religion-religioff.html]]/

Ya han pasado dos días y aun no tengo respuesta, supongo que aun esta reflexionando... Espero que les haya
gustado :D

*Actualizacion:* Me ha contestado, dijo que le a parecido muy interesante mis respuestas y que le agradaría
continuar el debate con alguna forma de comunicación en "tiempo real". Yo al menos no acepte continuar bajo
esos terminos, sí comenzo como mensajes privados por YouTube, puede continuar por ahí, ademas, tiene la misma
cantidad de ventaja que yo para formular bien sus preguntas y respuestas.

Ya veremos que pasa
