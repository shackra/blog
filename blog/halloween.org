#+TITLE:       Halloween y la incapacidad de discernimiento de algunos Católicos 
#+AUTHOR:      Jorge Araya Navarro
#+EMAIL:       elcorreo@deshackra.com
#+DATE:        2014-10-29 mié
#+URI: /blog/%y/%m/%d/halloween
#+KEYWORDS:    Iglesia Católica, rabietas
#+TAGS:        Iglesia Católica, rabietas
#+LANGUAGE:    en
#+OPTIONS:     H:3 num:nil toc:nil \n:nil ::t |:t ^:nil -:nil f:t *:t <:t
#+DESCRIPTION: Ahora se esta conociendo la verdad sobre Halloween, pero pocos católicos pueden "tragarla"

#+BEGIN_CENTER
[[http://www.catholicmemes.com/memes/happy-halloween/][http://i.imgur.com/CqYiSCQ.jpg]]
#+END_CENTER

Se acerca el Viernes, ¡Gracias a Dios ya casi es viernes!. También se acerca Halloween, y adelantándose varios días, comienza la pasarela de publicaciones de personas en redes sociales desalentando la celebración de esa fiesta, como es usual todos los años.

Varios medios Católicos de confianza han estado dedicando entradas en sus blogs sobre el verdadero origen de Halloween--pero acá no hablo del famoso origen celta--, sino más bien de su origen Católico. Exacto, así como lo lee usted, Halloween es Católico. Esta tendencia de los medios Católicos por difundir esas notas se a incrementado algo con respecto a años anteriores y es algo anterior a la creación del /holywin/, una celebración alternativa al Halloween creada por Católicos latinoamericanos.

No voy a gastar tiempo explicando que esas publicaciones son falsas y sin fundamento, porque no es así, están bien fundamentadas y explicadas por sacerdotes que conocen de estos temas; que conocen sobre historia, además de ocultismo. Tampoco las voy a colocar acá /verbatim/, si desean leerlas pueden revisar [[http://www.aleteia.org/es/sociedad/articulo/es-la-hora-de-que-los-catolicos-vuelvan-a-adoptar-halloween-5237957002788864][el articulo traducido al español por Aleteia]] y [[http://www.aleteia.org/es/sociedad/articulo/cual-es-la-relacion-entre-halloween-y-todos-los-santosfieles-difuntos-5840434345541632][su segunda parte]] (que trata sobre la relación de Halloween con las fiestas de Todos Los Santos/Fieles Difuntos), escrito originalmente por el padre Steven Grunow, CEO de [[http://www.wordonfire.org/][Word on Fire Catholic Ministries]] (sí, de la misma gente responsable por la [[http://www.seriecatolicismo.com][serie Catolicismo]]; el sacerdote en cuestión fue asistente de producción en la misma) y colaborador de EWTN. También pueden leer una nota escrita por el padre Jorge Luis Zarazúa Campa, F.M.A.P. en la pagina Apóstoles de la Palabra, en su [[http://www.apostolesdelapalabra.org/2014/10/halloween-una-fiesta-pagana-parte-1/][primer]] y [[http://www.apostolesdelapalabra.org/2014/10/halloween-una-fiesta-pagana-parte-2/][segunda]] partes respectivamente. éste otro sacerdote escribe también de manera regular, es de mi entender, en [[http://infocatolica.com/blog/infories.php/][InfoRIES: Red Iberoamericana de Estudios de las Sectas]].

En lo que sí voy a dedicar un par de lineas es a criticar la incapacidad de algunos Católicos para discernir lo falso de lo verdadero, y la falta de caridad que algunos poseen con quienes escriben estas publicaciones cuyo deseo es educar al resto del pueblo de Dios. En la sección de comentarios de esas notas, o en la sección de comentarios de los enlaces publicados en redes sociales, existe un cierto grado de rechazo a estas explicaciones sobre Halloween, pero el rechazo se manifiesta con paupérrima caridad, tratando de /masones infiltrados/ a los sacerdotes autores, definitivamente existen masones dentro de la Iglesia, pero Cristo no va dejar a su Esposa sola en éste problema, eventualmente las autoridades correspondientes dentro de la Iglesia descubren quienes son esas personas y les sancionan como corresponde, como ya ha sucedido; pero eso no se descubre cuando escriben a favor de las cosas que no te gustan, porque es verdad que a pocos les gusta Halloween. Incluso he visto personas pidiendo a medios Católicos retirar esas publicaciones; que problema cuando lo que esta en discusión se es asumido.

Para el Cristianismo, el discernimiento es algo súper importantísimo, la fe se puede estropear si en el camino se toman malas decisiones, como por ejemplo, contradecir las enseñanzas de la Iglesia porqué se ha discernido que la Iglesia no es el deposito de la Revelación plena, o porque la Iglesia /es una exagerada/ (¿Cuantos no hemos oído o leído esto en temas como la contracepción?). Y el tema de Halloween es algo trivial comparado con algunas cuestiones más graves en las cuales los Católicos pueden caer por un discernimiento pobre, no es complicado de entender, de hecho los sacerdotes anteriormente mencionados hacen una explicación muy articulada del tema cubriendo varios detalles permitiendo a la gente discernir si verdaderamente lo que se ha dicho de Halloween, fuera de la comunidad Católica de Estados Unidos, durante años es verdadero o falso. Si en las cosas triviales el discernimiento falla ¿Qué pasara con esa gente cuando tenga que decidir sobre cuestiones importantes que le cambiaran la vida?.

Ciertamente, el Halloween más confuso para los Católicos de habla hispana a sido éste. Entre más y más medios de confianza corren la voz sobre el origen verdadero de la fiesta, aparecen más y más comentarios por parte de Católicos en redes sociales con tono de confusión sobre el tema, /¿A quién creerle?/, escribe alguno. Buena pregunta epistemológica.

Y no me extraña que se pregunten /¿A quién creerle?/, existen varios testimonios que dan razones (y material de sabotaje) a los detractores
de Halloween para no disfrazar a los chiquillos ni dejarles salir a pedir confites, entre otras actividades tradicionales de la fiesta, acá algunos ejemplos:

#+BEGIN_HTML
<iframe width="700" height="525" src="https://www.youtube.com/embed/P37laTkxFgw?rel=0" frameborder="0" allowfullscreen></iframe>
#+END_HTML

#+BEGIN_HTML
<iframe width="700" height="394" src="https://www.youtube.com/embed/xM8cwR1Dkx4?rel=0" frameborder="0" allowfullscreen></iframe>
#+END_HTML

#+BEGIN_HTML
<iframe width="700" height="394" src="https://www.youtube.com/embed/YbhNfj5SE-Q?rel=0" frameborder="0" allowfullscreen></iframe>
#+END_HTML

Y como no mencionar las palabras del padre Gabriel Amorth, exorcista con más de 80 años de edad, [[http://noticias.lainformacion.com/asuntos-sociales/el-demonio-se-mete-en-todo-tambien-en-la-fiesta-de-halloween_0r7NhEEBRH9rN8Pg8TAUT2/][según el sitio de noticias lainformacion.com]]:

#+BEGIN_QUOTE
“Halloween es una trampa del demonio, que trata de que caigamos por todos los medios”, afirmaba el padre Gabriele Amorth, exorcista del Vaticano, en una entrevista el pasado año. “Se trata de una fiesta anticristiana y anticatólica (...) Es una idea del diablo, que tiene la intención de alterar los planes de Dios”, continuaba.
#+END_QUOTE

También he oído, de boca de una Católica (y eso es lo más preocupante), la supuesta historia que relata el origen de las /[[https://es.wikipedia.org/wiki/Jack-o%2527-lantern][Jack-o'-lantern]]/, una historia tras que inverosímil (porque implica que es posible saber el destino de la gente después de muerta, y en el caso de la historia, el protagonista había sido expulsado del infierno por ser extremadamente repugnante... ¿Qué rayos?), completamente herética y contra la doctrina Católica sobre el infierno (por lo que dije en el paréntesis anterior). Acá se puede apreciar lo nocivo que son algunas historias que se relatan para sabotear Halloween, presumiré, de boca de Protestantes (que en Estados Unidos son muy vocales contra Halloween, tanto como los Católicos de acá).

Bien ¿Cómo logramos explicar estos testimonios que hay contra Halloween como fiesta Católica en origen y esencia sin dañar el buen nombre de las personas que dan su testimonio? Para mi no a sido un proceso difícil, de hecho, es fácil si se sabe quién es el Demonio, que es lo que desea más que nada y cosas así.

Hay que recordar que los demonios tienen cierto grado de acción en el mundo, no son completamente libres para actuar. También hay que entender que ellos son superiores en inteligencia al hombre, incluso al hombre más inteligente que haya vivido jamás, la relación de inteligencia entre un ángel o demonio y un hombre es la misma que existe entre un hombre y un perro. Por ultimo también hay que recordar que el Demonio es un mentiroso, y que además desea ocupar el lugar que tiene Dios y todo lo que se relaciona con ÉL.

Mi explicación es la siguiente: El Demonio le ha sacado partido a una fiesta Católica que estaba siendo despojada de su esencia, debido a las falsedades que repetía la cultura Protestante circundante, cultura que es adversa a todas las cosas Católicas por pensarlas paganas, recuerden que los Estados Unidos de América fue colonizada primero por los Puritanos, y tiempo después aparecerían los primeros colonos Católicos, trayendo consigo sus tradiciones. Habiendo la cultura protestante hecho la mayor parte del trabajo en desfigurar la fiesta, y habiendo ayudado a resaltar también las características que ellos criticaban de la misma (y que son obvias para nosotros hoy día; me refiero a la celebración del miedo y los sustos como esencia de la fiesta, como el /de eso se trata todo esto/), lo siguiente que debía realizar el Demonio era reunir a las personas que le rendían culto y ordenarles que incrementaran la actividad satánica ésta fecha del año. ¡Y bingo! El Demonio a conseguido transformar una fiesta relacionada con Dios, a una fiesta que esta (supuestamente) contra ÉL y todo lo que de ÉL viene; un ejemplo más de avaricia, muy propio viniendo de un ángel caído.

Pienso que mi explicación tiene sentido sólo porque hay gente con más conocimiento, como el histórico y ocultista, suficientes como para articular una explicación completa ante tanto mito que hay alrededor de Halloween, y que es más detallada que algunos testimonios o el  repetir de argumentos de fuentes poco fiables (¿En serio quien va a confiar en lo que salga de la boca de un satanista, o en lo que haga en cierta fecha, como si fuera santa palabra o la acción tuviera fuerza como para darle un significado especial a un día del año? O ¿de un neo-pagano cuyo entorno cultural al que desea rendir culto, como lo es el paganismo antiguo, es inexistente?) por parte de buenas personas (como el padre Amorth o el padre Pedro). Y tiene sentido porque además es armonioso con lo que dichas autoridades en el tema han explicado, y lo que sabemos del Demonio. En otras palabras, mi explicación sobre esos detalles que faltaban por explicar no aporta nada nuevo, solamente intenta /cerrar el circulo/, como dicen.

Educar a la gente en la verdad es bueno, por ello pido a las personas que están en contra de Halloween, sea por la razón que sea, ya no continuar usando las falsedades que se han dicho para sabotear esa fiesta, por el mero hecho de ser falsedades merecen eso, no volver a ser repetidas (a menos claro, que usted pueda probar fehacientemente lo contrario y refutar lo que expusieron dichos sacerdotes). Además, pido esto porque se siembra la confusión en las demás personas.

#+BEGIN_CENTER
{{http://i.imgur.com/qK1GlDn.jpg}}
Pienso que la cita es relevante al párrafo anterior.
#+END_CENTER

Estoy de acuerdo que lo que hoy se celebra es algo torcido, pero sabiendo la verdad podemos recuperar la fiesta. Hay que disentir que por el mero hecho de salir disfrazados a pedir confites se este participando de alguna manera con algo satánico. Las cosas Católicas son de teflón, a Halloween no se le va a adherir el satanismo en la misma medida que a la Santa Misa no se le pega nada satánico por culpa de las Misas Negras. Y precisamente porque las cosas Católicas son de teflón, son recuperables, y la única manera de recuperar Halloween es pregonando la verdad. Decir que salir a la calle disfrazado y pidiendo confites no tiene nada de dañino sin haber explicado primero porqué /All Hollow's Eve/ o Halloween no es una fiesta de origen pagano o satánico es, a mi juicio, igual de dañino que repetir los mitos de siempre, esto porque la explicación opera alrededor de la idea de que Halloween tiene ésta precedencia adversa al Catolicismo. Por favor cuiden ese detalle también.

¿Qué hay con la iniciativa Holywin? Pues, que tiene [[http://www.elijahlist.com/words/display_word/3534][origen protestante]], aunque eso no lo hace malo. Lo que hace al Holywin malo es ser una alternativa Cristiana a lo que ya es, por origen y verdadera naturaleza, Cristiano. Pienso que es un esfuerzo innecesario que se podría gastar en educar en la verdad a las personas que tenemos alrededor. Esta recomendación la doy por mis estudios en diseño publicitario y tratando la festividad de Halloween como si fuese una marca, si una marca de repente cambia su imagen o nombre sin avisar a sus clientes, los van a perder porque ellos pensaran que es otra marca, Halloween y Holywin son dos cosas distintas desde el punto de vista de la persona que conoce nada del tema. Además, Holywin por nombre no tiene nada que ver con la festividad de Todos los Santos, esto no es así con el termino Halloween ya que es una contracción de el termino en inglés All Hallow's Eve; esto sucede todo el tiempo (o a través del tiempo para ser exacto) con el idioma (de ahí que la Iglesia use el Latín como su idioma, es excelente cuando se quiere definir dogmas porque preserva lo que la Iglesia desea expresar), por eso existe la palabra /adiós/ que es una contracción de las palabras /A Dios/, obviamente los dos términos no significan lo mismo ni se usan igual pero es obvia que existe una relación entre ambas... me he pasado de filólogo. Si no se desea usar el termino Halloween, yo recomendaría usar la traducción /Víspera de Todos los Santos/ por encima de Holywin.

Y, como dicen en Internet: Ahora Que Ya Ustedes Saben, disfruten a la Madre Angélica celebrando el Halloween de 1994:

#+BEGIN_HTML
<iframe width="700" height="394" src="https://www.youtube.com/embed/PxEvkqNayTk?list=PL5uhTQe5h_QeWBS5N0fpeEazoEijgz9ML" frameborder="0" allowfullscreen></iframe>
#+END_HTML
