#+TITLE: La verdadera causa de la renuncia del Papa
#+AUTHOR: Jorge Araya Navarro
#+EMAIL: elcorreo@deshackra.com
#+DATE: 2013-02-13
#+LANGUAGE: en
#+URI: /blog/%y/%m/%d/la-verdadera-causa-de-la-renuncia-del-papa
#+OPTIONS: H:3 num:nil toc:nil \n:nil ::t |:t ^:nil -:nil f:t *:t <:t
#+KEYWORDS: Iglesia Católica, opinión
#+TAGS: Iglesia Católica, opinión

http://i.imgur.com/iqJ8N7b.jpg

Autor original: OEHD ([[https://oehd.wordpress.com/2013/02/12/siempre-renuncias-benedicto/][oehd.wordpress.com]])

Tengo 23 años y aún no entiendo muchas cosas. Y hay muchas cosas que no se pueden entender a las 8:00am cuando te hablan para decirte escuetamente: "Daniel, el papa dimitió." Yo apresuradamente contesté: "¿Dimitió?". La respuesta era más que obvia, "Osea renunció, ¡Daniel, el papa renunció!"

El Papa renunció. Así amanecerán sin fin de periódicos mañanas, así amaneció el día para la mayoría, así de rápido perdieron la fe unos cuantos y otros muchos la reforzaron. Y que renunciara, es de esas cosas, que no se entienden.

Yo soy católico. Uno de tantos. De esos que durante su infancia fue llevado a misa, luego creció y le agarró apatía. En algún punto me llevé de la calle todas mis creencias y a la Iglesia de paso, pero la Iglesia no está para ser llevada ni por mí, ni por nadie (ni por el Papa). En algún punto de mi vida, le volví a agarrar cariño a mi parte espiritual (muy de la mano con lo que conlleva enamorarse de la chavita que va a misa, y dos extraordinarios guías llamados padres), y así de banal, y así de sencillo, recontinué un camino en el que hoy digo: Yo soy católico. Uno de muchos, si, pero católico al fin. Pero así sea un doctor en teología, o un analfabeta de las escrituras (de esos que hay millones), lo que todo mundo sabe es que el Papa es el Papa. Odiado, amado, objeto de burlas y oraciones, el Papa es el Papa, y el Papa se muere siendo Papa.   Por eso hoy cuando amanecí con la noticia, yo, al igual que millones de seres humanos..nos preguntamos ¿porqué?. ¿Porqué renuncia señor Ratzinger?. ¿Le entró el miedo?. ¿Se lo comió la edad?. ¿Perdió la fe?. ¿La ganó?. Y hoy, después de 12 horas, creo que encontré la respuesta: El señor Ratzinger, ha renunciado toda su vida.

Así de sencillo.

El Papa renunció a una vida normal. Renunció a tener una esposa. Renunció a tener hijos. Renunció a ganar un sueldo. Renunció a la mediocridad. Renunció a las horas de sueño, por las horas de estudio. Renunció a ser un cura más, pero también renunció a ser un cura especial. Renunció a llenar su cabeza de Mozart, para llenarla de teología. Renunció a llorar en los brazos de sus padres. Renunció a teniendo 85 años, estar jubilado, disfrutando a sus nietos en la comodidad de su hogar y el calor de una fogata. Renunció a disfrutar su país. Renunció a tomarse días libres. Renunció a su vanidad. Renunció a defenderse contra los que lo atacaban. Vaya, me queda claro, que el Papa fue un tipo apegado a la renuncia.

Y hoy, me lo vuelve a demostrar. Un Papa que renuncia a su pontificado cuando sabe que la Iglesia no está en sus manos, sino en la de algo o alguien mayor, me parece un Papa sabio. Nadie es más grande que la Iglesia. Ni el Papa, ni sus sacerdotes, ni sus laicos, ni los casos de pederastia, ni los casos de misericordia. Nadie es más que ella. Pero ser Papa a estas alturas del mundo, es un acto de heroísmo (de esos que se hacen a diario en mi país y nadie nota). Recuerdo sin duda, las historias del primer Papa. Un tal..Pedro. ¿Cómo murió? Si, en una cruz, crucificado igual que a su maestro, pero de cabeza. Hoy en día, Ratzinger se despide igual. Crucificado por los medios de comunicación, crucificado por la opinión pública y crucificado por sus mismos hermanos católicos. Crucificado a la sombra de alguien más carismático. Crucificado en la humildad, esa que duele tanto entender. Es un mártir contemporáneo, de esos a los que se les pueden inventar historias, a esos de los que se les puede calumniar, a esos de los que se les puede acusar, y no responde. Y cuando responde, lo único que hace es pedir perdón. 'Pido perdón por mis defectos'. Ni más, ni menos. Que pantalones, que clase de ser humano. Podría yo ser mormón, ateo, homosexual y abortista, pero ver a un tipo, del que se dicen tantas cosas, del que se burla tanta gente, y que responda así..ese tipo de personas, ya no se ven en nuestro mundo.

Vivo en un mundo donde es chistoso burlarse del Papa, pero pecado mortal burlarse de un homosexual (y además ser tachado de paso como mocho, intolerante, fascista, derechista y nazi) [1]. Vivo en un mundo donde la hipocresía alimenta las almas de todos nosotros. Donde podemos juzgar a un tipo de 85 años que quiere lo mejor para la Institución que representa, pero le damos con todo porque "¿con qué derecho renuncia?". Claro, porque en el mundo NADIE renuncia a nada. A nadie le da flojera ir a la escuela. A nadie le da flojera ir a trabajar. Vivo en un mundo donde todos los señores de 85 años están activos y trabajando (sin ganar dinero) y ayudan a las masas. Si, claro.

Pues ahora sé Señor Ratzinger, que vivo en un mundo que lo va a extrañar. En un mundo que no leyó sus libros, ni sus encíclicas, pero que en 50 años recordará cómo, con un simple gesto de humildad, un hombre fue Papa, y cuando vio que había algo mejor en el horizonte, decidió apartarse por amor a su Iglesia. Va a morir tranquilo señor Ratzinger. Sin homenajes pomposos, sin un cuerpo exhibido en San Pedro, sin miles llorándole aguardando a que la luz de su cuarto sea apagada. Va a morir, como vivió aún siendo Papa: humilde.

Benedicto XVI, muchas gracias por renunciar.

[1] O mejor aun, no burlarse pero si hablarles la verdad sobre lo malsano que es ese estilo de vida, hablarles del amor de Jesús tiene hacia ellos a pesar de no haber escogido tener sentir esa atracción sexual hacia el mismo sexo, y al final, las burlas y los nombres... ¡Ni por una ni por otra!.
