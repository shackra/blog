#+TITLE: J.R.R Tolkien sobre el "matrimonio" gay
#+AUTHOR: Jorge Araya Navarro
#+EMAIL: elcorreo@deshackra.com
#+DATE: 2013-06-26
#+LANGUAGE: en
#+URI: /blog/%y/%m/%d/j-r-r-tolkien-sobre-el-matrimonio-gay
#+DESCRIPTION:
#+OPTIONS: H:3 num:nil toc:nil \n:nil ::t |:t ^:nil -:nil f:t *:t <:t
Lang: es
#+KEYWORDS: política, opinión, traducción
#+TAGS: política, opinión, traducción

#+BEGIN_CENTER
[[http://i.imgur.com/r4cBye3.jpg]]
#+END_CENTER

Autor original: Mark Shea ([[http://www.patheos.com/blogs/markshea/2013/06/j-r-r-tolkien-on-gay-marriage.html][Catholic and enjoying it!]])

"Soy un Cristiano, y de hecho un Católico Romano, así que no espero que la "historia" sea otra cosa que una "larga derrota" - a pesar de que contiene algunas muestras o atisbos de victoria final."

No puedo decir que estoy muy sorprendido con respecto a los SCOUTS. Por lo general refleja la cultura en términos generales, ya sea con Dred Scot o Roe. Algunas veces va un poco adelante, otras veces se queda atrás cuando Nosotros el Pueblo decide correr en estampida bajo el látigo de la Cultura Pop o el /Big Think/.

Voy a señalar que con esta decisión John Roberts aporta la cereza en el pastel de basura de la Administración Bush. Él fue, por supuesto, absolutamente la razon por la que tuvimos que votar por George W. Bush y apoyar su catastrófica e injusta guerra y aclamar el Patriot Act y hacernos de la vista gorda con mala gestión económica por él y Dick "El Déficit No Importa" Cheney y vender nuestras almas para excusar la tortura. Porqué por más que la zanahoria del Cambiar la Corte ha estado balanceándose delante de nosotros por más de 30 años. Ciertamente, hemos estado atacando viciosamente a aquellos quienes rechazaron votar por los males reales de hoy con la esperanza de que alguna imaginaria Corte anti-Roe aparezca mañana. ¿El mundo real es el resultado de votar "realistamente" por los Republicanos en lugar de negarse a hacerlo? Bueno, incluso sabiendo que el mismo Roberts **dijo** "Roe es una ley establecida" nos decimos a nosotros mismos que él en realidad no quiso decir eso y sencillamente quiso justamente decir esas cosas para deshacerse de los Demócratas.

Desde entonces, sus tres logros han sido señales de aprobación de Obamacare, el [[http://es.wikipedia.org/wiki/Caso_Ciudadanos_Unidos_contra_Comisi%C3%B3n_de_Elecciones_Federales][Citizen's United]] ("¡Las grandes corporaciones también son personas!") y ahora esto. En resumen, Oh Conservadores Sociales, enviamos a nuestros niños ha sufrir daño cerebral, una alza en el ratio de suicidios militares, negligencia de la administración del sistema médico para veteranos, enormes índices de desempleo entre veteranos de una nación agradecida con ellos, y un rescate a la corporatocracia por **éste** tipo.

Estoy **tan** cansado de votar "realistamente". Voy a votar por personas quienes no me digan hacer un gran mal porqué un bien mayor saldrá de eso y por nadie más. Y si no existe nadie más. Voy a encontrar una forma de enfrentar la cultura que no sea políticamente. Como pinta todo esto, pongo atención enormemente a la política por la misma razón que un general trata de estar al tanto de los movimientos de las tropas enemigas. La Clase Dirigente no son nuestros amigos.

En cuanto a la decisión sobre el propio "matrimonio" gay: Si el Cesar quiere engañarse a sí mismo con su papeleo y pretender que existe tal cosa como el "matrimonio" gay, nunca me he creído capaz de detenerlo por mucho tiempo. Y no es que el Cesar tenga el crónico habito de mentirse a sí mismo y a los demás. El problema sera, por supuesto, cuando el Cesar vaya tras aquellos quienes se opusieron ha unirse a él en su mentira pretendiendo que existe el "matrimonio" gay. Es claro desde hace mucho tiempo que el despiadado narcisismo de la comunidad gay no podrá decaer con mera tolerancia. La razón para hacer el "matrimonio" gay una cuestión de ley y no de fingidas ceremonias privadas es porqué en primer lugar es una demanda que todos en el universo sean forzados a aceptar esta narcisista fantasía y castigados si no lo están. Si alguna pareja (o trió o cuarteto) quiere fingir que se casan con alguien del mismo sexo, o con un almacén, o con un delfín o un perro o una montaña rusa o consigo mismos (todas estas cosas que ya han sucedido, googlealo), es un país libre. Nadie les esta deteniendo para fingir que lo hacen y puede de hecho "definir el matrimonio como sea que guste de acuerdo a su conciencia." Pero estamos concientes que cuando haces eso, estas diciendo que el "matrimonio" significa cualquier cosa. Y cuando haces eso, inevitablemente acabaras diciendo que el "matrimonio no significa nada."

Y con el vació que deja la perdida de todo significado la palabra "matrimonio" solo puede usarse poder duro y puro con el fin de obligar a la gente a aceptar, no "lo que el matrimonio significa de acuerdo con la conciencia de cada quien" sino lo que el Cesar nos obliga creer a punta de pistola, porqué esa es la forma en la que el Cesar impone su voluntad. Pero eso parece estar bien con la Cultura Gay que ha sido bastante claro que **le gusta** aplicar poder draconiano contra aquellos que niegan creer en sus corazones y profesar con sus bocas que el homosexo es la fuente y cúspide de todo lo noble, bueno, puro y justo. Así, por ejemplo, éste [[http://libertycounsel.com/wp-content/uploads/2013/05/LGBT_tips_for_managers.pdf][memo del Departamento de Justicia]] para empleadores nos da instrucciones para cuando un compañero de trabajo salé del closet, el silencio tolerante no es suficiente. ¡Usted. DEBE. Aprobarlo! ("NO juzgue o permanezca en silencio. El silencio sera interpretado como desaprobación.")

Esta es la forma que tienen las cosas que vendrán [1]. Como un lector gay en otro blog dijo, "Tu tienes que hacer lo que NOSOTROS digamos ahora." Hacia allá se dirige esto: hacia la búsqueda de bases legales, no para la tolerancia, pero para aplastar a todos aquellos que no se unirán a la pretensión que pueda haber tal cosa como el "matrimonio" gay y como es referido el homosexo como pecado.

Y aun así, voy a ir más lejos y lo diré: No existe tal cosa como el "matrimonio" gay. Tienes todos los atavíos, disfruta de todas los beneficios civiles (No tengo ningún interés en intentar detenerte). Pero de la misma forma que bautizar a un pingüino no lo convierte al pingüino en un Cristiano, lo mismo ocurre con todas las palabras y tener el pastel y toda la estética no hace que sea un matrimonio. Declino en pretender que sí lo hace. Y nadie va a hacer cambiar de opinión nunca.

[1] originalmente decía «That is the shape of things to come», esta frase es también titulo de unos de los libros del famoso escritor H.G. Wells.
