#+TITLE:       No pierdas más trabajos por culpa de tú inglés
#+AUTHOR:      Jorge Araya Navarro
#+EMAIL:       elcorreo@deshackra.com
#+DATE:        2015-09-08 mar
#+URI:         /blog/%y/%m/%d/no-pierdas-mas-trabajos-por-culpa-de-tu-ingles
#+KEYWORDS:    BitTorrent, idiomas, inglés
#+TAGS:        BitTorrent, idiomas, inglés
#+LANGUAGE:    en
#+OPTIONS:     H:3 num:nil toc:nil \n:nil ::t |:t ^:nil -:nil f:t *:t <:t
#+DESCRIPTION: Un recurso "gratuito" que puede ayudar a mejorar tu nivel de inglés

#+ATTR_HTML: :align left
http://i.imgur.com/Y33oNYq.jpg

* ACTUALIZACIÓN
El /torrent/ original que recomendé trae muchos archivos, como dije. Y de entre los más valiosos y que valen la pena ha sido únicamente el audio libro /Pronounce it Perfectly in English/, entonces decidí mantener las cosas simples, he hice un /torrent/ con los archivos de ese audio libro. Acá esta:

#+BEGIN_SRC emacs-lisp :exports results :results value html
  (shackra/owp-descargas "Nueva lista de descarga" '(("Pronounce It Perfectly in English, 2nd Edition"
                          "408E080C91C4549DDD4F75400F9AC004A745FC36")
                         ("Mastering the American Accent"
                          "7BD88DFE770AAB197766BE707194FD591B2058D3")))
#+END_SRC

Por favor ayuden sembrando este /torrent/, tratare de dejar mi portátil encendida por las noches para que se comparta con otros usuario y así asegurarme que la máxima cantidad de personas lo tengan. Cabe decir que *arreglé el archivo PDF un poco y he cambiado los nombre de archivos de los MP3s para evitar confundir a los estudiantes*.

También he agregado otro /torrent/ con la misma temática a ésta entrada, [[http://amzn.com/0764195824][/Mastering the American Accent/]], porque he leído que es excelente material que se podría usar en conjunto con /Pronounce it Perfectly in English/ (y la verdad tener una clasificación de 4.6 de 5 estrellas en Amazon con 137 votos emitidos lo pone uno a pensar; esa descarga no sera un desperdicio de espacio en tu disco duro).

¡Buen provecho!
* Entrada original
En estos dos meses aproximadamente, he aplicado para puestos de trabajo en el área de servicio al cliente. Ambas empresas para las cuales aplique me sorprendieron con su interés al punto de realizarme entrevistas telefónicas.

¡Genial! ¿No? ya casi tengo el puesto en la bolsa ¿Qué podría salir mal?

Las entrevistas, ellas son realizadas en el idioma inglés. Y mi inglés no es fluido, a pesar de pasar 80% del tiempo expuesto a este idioma a través de la computadora e Internet. He perdido ambas oportunidades (¿casi?) seguras de trabajo, y al verdad necesitaba arreglar mi situación con el idioma lo más pronto, y de ser posible, que me cueste poco o nada económicamente hablando (No por ser tacaño, si no porque no tengo la "harina para hacer el pan").

Voltee los ojos a un sitio que nunca me ha fallado, BitTorrent, estoy casi seguro que alguien en alguna parte esta /sembrando/ archivos que me ayudaran a remediar mi problemas de pronunciación y vocabulario, de hecho, acá hay un torrent:

#+BEGIN_SRC emacs-lisp :exports results :results value html
  (shackra/owp-descargas "Descargas" '(("English Pronunciation Books and Audio Books Collection - Mantesh"
                          "ED45C845B0E478738ED0B9BA9C5140AA3468FB8F")))
#+END_SRC

Trae muchísimos archivos, pero creo que lo más interesante son sus audio libros, el primero [[http://amzn.com/0764177494][/Pronounce it Perfectly in English/]] tiene muy buenas calificaciones por parte de compradores en Amazon. Lo mismo parece suceder con [[http://amzn.com/0521678080][/Pronunciation Pairs Student's Book with Audio CD/]], este torrent parece ser una mina de oro, yo que usted, aprovecho :).
