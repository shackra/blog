#+TITLE:       Prueba definitiva de que amo a mi esposa
#+AUTHOR:      Jorge Araya Navarro
#+EMAIL:       elcorreo@deshackra.com
#+DATE:        2015-05-10 dom
#+URI:         /blog/%y/%m/%d/prueba-esposa
#+KEYWORDS:    castidad
#+TAGS:        castidad
#+LANGUAGE:    es
#+OPTIONS:     H:3 num:nil toc:nil \n:nil ::t |:t ^:nil -:nil f:t *:t <:t
#+DESCRIPTION: Incluso antes de siquiera conocerla o casarme con ella

#+BEGIN_HTML
<a href="https://www.flickr.com/photos/mejiaperalta/6173781645" title="IMG_8489Berm_Mart by Jorge Mejía peralta, en Flickr"><img src="https://c3.staticflickr.com/7/6169/6173781645_9eaaaa1481_b.jpg" width="1024" height="683" alt="IMG_8489Berm_Mart"></a>
#+END_HTML

#+BEGIN_HTML
<div id="fb-root"></div><script>(function(d, s, id) {  var js, fjs = d.getElementsByTagName(s)[0];  if (d.getElementById(id)) return;  js = d.createElement(s); js.id = id;  js.src = "//connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.3";  fjs.parentNode.insertBefore(js, fjs);}(document, 'script', 'facebook-jssdk'));</script><div class="fb-post" data-href="https://www.facebook.com/jorgejavieran/posts/946304112057430" data-width="500"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/jorgejavieran/posts/946304112057430"><p>Querida esposa,Te amo mucho, incluso sin saber d&#xf3;nde estas, qui&#xe9;n eres o cu&#xe1;ndo entraras en mi vida. Por siempre vuestro,Jorge.</p>Posted by <a href="https://www.facebook.com/jorgejavieran">Jorge Araya Navarro</a> on <a href="https://www.facebook.com/jorgejavieran/posts/946304112057430">Lunes, 4 de mayo de 2015</a></blockquote></div></div>
#+END_HTML

Por eso, me guardo para ella ;).
