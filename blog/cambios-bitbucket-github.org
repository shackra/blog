#+TITLE:       enviar cambios del mismo proyecto a bitbucket y a GitHub
#+AUTHOR:      Jorge Araya Navarro
#+EMAIL:       elcorreo@deshackra.com
#+URI: /blog/%y/%m/%d/%t/
#+DATE:        2013-03-20 
#+KEYWORDS:    software libre, Bitbucket, Github
#+LANGUAGE:    en
#+OPTIONS:     H:3 num:nil toc:nil \n:nil ::t |:t ^:nil -:nil f:t *:t <:t
#+TAGS:        software libre, Bitbucket, Github
#+DESCRIPTION: Enviar cambios de un repositorio Mercurial a un repositorio Git con HGGIT

Adoro [[http://en.wikipedia.org/wiki/Mercurial_%28software%29][Mercurial]] y detesto [[http://en.wikipedia.org/wiki/Git_%28software%29][Git]]. Más que nada porque Mercurial es más sencillo de usar y fue el primer CVD que
use alguna vez para profesionalizarme más en esto del desarrollo de software. Mi primer sitio para hospedar
mis proyectos de software libre es [[http://es.wikipedia.org/wiki/Bitbucket][Bitbucket]] que es, por mucho, el mejor sitio para hospedar tus proyectos
usando Mercurial (aunque hace poco integraron Git, así que puedes escoger qué usar en Bitbucket); aunque no es
secreto para nadie que [[http://es.wikipedia.org/wiki/GitHub][GitHub]] es algo así como /el lugar donde tus proyectos de software deben estar si
quieres que hagan muchos forks de tus proyectos™/. En fin, no uso Git y no tenia ningun proyecto hospedado
ahí, pero averiguando sobre el uso de Travis-ci con Bitbucket [[http://pythonwise.blogspot.com/2012/05/using-travis-ci-with-bitbucket.html][me encontré una interesante nota]] de Miki Tebeka
en la cual nos enseña un interesante truquillo para enviar los cambios de tu proyecto al mismo repositorio en
dos sitios de hospedaje distintos.

En principio vamos a necesitar el /plugin/ [[http://hg-git.github.com/][hggit]] que nos permite convertir cambios entre Git y Mercurial. Ya
sabes como instalarlo: =pip install hg-git=

Lo siguiente seria editar la configuración del repositorio de tu proyecto, tienes que habilitar dos /plugins/
para Mercurial y agregar un /hook/, edita el =.hg/hgrc= de tu proyecto:

#+BEGIN_EXAMPLE     
  # ...
  [extensions]     
  hgext.bookmarks =
  hggit =

  [hooks]     
  outgoing = hg push git+ssh://git@github.com/cuentagithub/proyecto.git || true
#+END_EXAMPLE 

(el =|| true= es necesario porque a veces =hg push= termina correctamente con un valor distinto a cero).
Necesitas usar un marcador (o bookmark) para la rama /default/ de tu proyecto, esto es porque las ramas son
conceptos distintos en Mercurial y Git, además, así le haces la vida más fácil a hggit(?). ejecuta el comando
=hg bookmark -r default master=. Y eso es todo :). Si deseas saber cómo usar Travis-ci con tu proyecto que
ahora se hospeda en GitHub, ¡lee la [[http://pythonwise.blogspot.com/2012/05/using-travis-ci-with-bitbucket.html][nota de Miki Tebeka]] en su blog!
