#+TITLE: Yo también hablo como tico: 4 maneras en las que el debate sobre el matrimonio gay ha sido encajonado
#+AUTHOR: Jorge Araya Navarro
#+EMAIL: elcorreo@deshackra.com
#+LANGUAGE: en
#+URI: /blog/%y/%m/%d/%t/
#+DESCRIPTION:
#+OPTIONS: H:3 num:nil toc:nil \n:nil ::t |:t ^:nil -:nil f:t *:t <:t
#+DATE: 2013-02-27
#+KEYWORDS: Iglesia Católica, política, opinión, traducción
#+TAGS: Iglesia Católica, política, opinión, traducción

Autor original: Marc Barnes ([[http://www.patheos.com/blogs/badcatholic/2012/05/4-ways-the-gay-marriage-debate-has-been-rigged.html][BadCatholic]])

Cristianos, Católicos, hombres y mujeres con AMS, defensores del Matrimonio Gay y opositores del mismo:

*El debate sobre el Matrimonio Gay no es un debate.* Se ha encajonado, encuadrado y devaluado en una serie de consignas pre-ordenadas para que cada "bando" grite, hasta el punto en donde comenzar a hacer esto es menos que tener argumentos y es más como recibir golpes en la cara por un furioso Snorlax.

[[http://wp.patheos.com.s3.amazonaws.com/blogs/badcatholic/files/2012/05/snorlax.png]]

No hay nada más deprimente que esto, por la simple razón de que todos los involucrados estan obligados a defender abstracciónes y consignas en lugar de las Cosas en sí; obligados a ser las extrañas representaciones de "Movimientos" que no existen, en lugar de ser los seres humanos que sí existen. Entonces, aquí van las 4 maneras en las que el debate sobre el matrimonio gay a sido encajonado, (N.T.: Ni idea de como traducir lo que va acá a español):

** 1. ¡ODIO!

Esto podría realmente impactarle: De los 28 estados en Estados Unidos donde iniciativas para hacer modificaciones constitucionales donde se defina el matrimonio como la union de un hombre y una mujer que fueron enviadas a referendum, los votantes de todos los 28 estados votaron a favor de dichas modificaciones. La gente común no esta de acuerdo con el Matrimonio Gay.

Ahora la razón de que esta noticia le parezca chocante es, que si usted esta muy metido en el Gran Falso Debate, sabra que aquellos que se oponen al matrimonio gay lo hacen porque todos ellos estan llenos de ¡Odio! hacia las personas gays. Debemos concluir entonces, que la mayoria de Norteamericanos (N.T.: O Costarricenses) son personas odiosas.

#+CAPTION: Capitan Intolerancia, la manera Norteamericana.
http://i.imgur.com/F3N60CR.png

Ahora, no soy optimista: No tengo una creencia excesivamente optimista de que todos los seres humanos son siempre lo mejor de ellos mismos. Pero la idea de que el hombre o mujer promedio que no apoya el matrimonio gay lo hace por puro odio (eso es, "hostilidad intensa y aversión usualmente derivados del miedo, la furia o la sensación de ser dañados") es simplemente una tontería. Sin ofender, pero la mayoria de personas no tienen la energia necesaria para mantener un sentimiento de hostilidad intensa contra conceptos abstractos. Sin embargo, este absolutismo moral es el /modus operandi/ del debate sobre el Matrimonio Gay. Y así:

#+CAPTION: No siempre me dicen la personificacion pura del mal, pero cuando lo hacen es porque estoy en el lado equivocado del debate sobre el matrimonio gay
http://i.imgur.com/Dod6Bqk.png

En serio, Yo probablemente podria abogar por el asesinato de Judios en este blog y nunca recibir una fracción de los insultos ridiculamente intensos que recibo cuando menciono la palabra G-A-Y. (Si tiene la oportunidad, y quiere aprender precisamente cómo abandonar por rabia, revise los comentarios en mi [[http://www.patheos.com/blogs/badcatholic/2012/05/the-christian-case-for-gay-marriage-the-smackdown.html][ultima publicación]].)

La respuesta a esta tonteria de kindergarden es simple, sólo requiere hacer definiciones. Si el amor es abordado en su sentido clasico --- buscar siempre el bienestar del ser amado --- entonces es muy dificil decir que aquellos qe se oponen al matrimonio gay lo hacen con un odio epileptico que los hace retorcerse en el piso.

Eso porque ya sabemos que el estilo de vida gay conduce a un [[http://www.collectionscanada.gc.ca/webarchives/20071120000549/http://www.phac-aspc.gc.ca/aids-sida/populations_e.html][mayor riesgo de contagiarse de VIH]], [[http://www.biomedcentral.com/1471-244X/8/70/][depresión, abuso de sustancias]], y generalmente a [[http://ije.oxfordjournals.org/content/26/3/657.abstract][menores expectativas de vida]]. Oponerse a la normalización de un estilo de vida que conduce a la degradación de la persona humana --- especificamente a la persona con AMS --- no es odiar, pero sí amar. No es un amor que muchas personas quicieran, no obstante, es el deseo por el bienestar de la persona amada.

Para ser claro, No creo que la mayoria de personas en Norteamerica (N.T.: o en Costa Rica) usen ese argumento cuando se oponen al matrimonio gay. La mayoria parecen tener un concepto general, innato, o religioso que les ayuda a reflexionar sobre el tema, y aunque esten equivocados o no, votan en consecuencia. Absolutamente, muchas personas ignoran porque exactamente se oponen al matrimonio gay --- así es cómo obtienes argumentos que no son argumentos como "El matrimonio gay no esta en la Biblia", o la evasion de la "homosexualidad es innatural".

¿Pero al menos podemos en ultima instancia ser honestos, y admitir que son muy poquitas las personas quienes desean mantener la definicion de matrimonio como la union entre hombre y mujer, y lo hacen con ardiente odio? Es muy posible amar al hombre y a la mujer que siente atraccion por el mismo y aun así oponerse al matrimonio gay. Carajo, es muy posible /ser/ un hombre que siente atraccion por personas del mismo sexo y oponerse al matrimonio gay, cómo muchos de los seguidores de éste blog les alegrara informarte.

Pero como suele suceder, es más sencillo bautizar a tus oponentes en la Westboro Baptist Church que escucharles, y de la misma forma es más sencillo tildar a personas con atracción por el mismo sexo de "maricones" o "pecadores" que amarles. Y así sucesivamente.

** 2. ¡QUE SE JODAN LAS DEFINCIONES TODOS SABEN DE QUÉ ESTOY HABLO ¿NO? SÍ PALABRAS WOOJO!

No sé cuando sucedió, pero el lenguaje en que el debate esta actualmente enmarcado ha le sido enteramente eliminado de todo sentido y significado. Somos todos hombres huecos, hombres de peluche, y nuestras palabras tienen todo el peso de susurros y paja.

El Cristiano afirma que apoya el "Plan de Dios para el matrimonio" y ciertamente sera maldecido cómo teocrata por la frase (N.T.: Justo Orozco ¿Nadie?). Pero lo peor del problema es que --- en general --- él no tiene ni la menor idea cual es el plan de Dios para el matrimonio. Los Cristianos se [[http://www.christianpost.com/news/study-christian-divorce-rate-identical-to-national-average-31815/][divorcia y vuelve a casar al mismo ratio que todos los demas]] a pesar de que Cristo fue absolutamente claro al decir que "Por tanto lo que Dios ha unido, que ningún ser humano lo separe." (Marcos 10:9 PDT). Solemos usar la frase, "El plan de Dios," pero lo hacemos como un concepto abstracto. ¿Qué tan a menudo recordamos que el plan de Dios para el matrimonio es la sumisión de la mujer y la crucifixión del hombre, un icono que revela como Cristo abraza Su Iglesia?

Lo siento, fue un espasmo Católico. Tratare de explicarme mejor:

http://i.imgur.com/BGuw0ee.jpg

Continuemos entonces:

Ahora el defensor del matrimonio gay, por otra parte, dirá que el matrimonio se trata sobre el amor, no del genero. De nuevo, el problema aquí no es que la súper-pomposa frase sea cierta o falsa, sino que es totalmente vacia. No tiene la capacidad de contener veracidad o falsedad --- un intento de definir cualquiera de los sustantivos involucrados demolerá hasta sus cimientos todo el asunto.

El matrimonio se deja indefinido. En efecto, si se determina que el matrimonio es mera unión, la pregunta surge entonces: ¿Por qué entonces no es considerado matrimonio cualquier comunión de personas? Se podría limitar el matrimonio al reconocimiento del amor mutuo, consensual entre dos personas, pero entonces se tendría que definir el amor. [[https://www.youtube.com/watch?v=VVmbhYKDKfU][¿Qué es amor? (Nena, no me hieras.)]], Si el amor se define como siempre a sido definido --- Buscar el bienestar de la persona amada --- se hace difícil justificar la normalización de un estilo de vida que no tiene (como he mencionado anteriormente) la finalidad de conducir al ser humano hacia su bienestar.

Si el amor es simplemente un sentimiento, entonces es subjetivo al igual que ese sentimiento. El amor puede ser /x,/ si es algo que tu "solamente conoces cuando tu lo sientes." Entonces diremos algo como: espérese un toque, ¿Qué clase de amor? Porque existen hermanos quienes sienten un sentimiento /x/ el uno hacia el otro. Ellos lo llaman amor, un sentimiento mutuo y consensual, indiferente del genero. Así tendremos que definir amor como especificamente amor /erotico/. Pero entonces tendriamos que definir lo erótico.

http://i.imgur.com/iAeBJ3W.png

Pues en este caso lo erótico ya no puede significar lo sexual, como el amor sexual --- por su naturaleza biológica --- contiene el objetivo inherente de la reproducción y la unidad de la pareja. Así que la sexualidad debe ser redefinido para sólo contener el aspecto unitivo del sexo --- no el reproductivo. Y en este momento hay al menos 20 formas en las que uno podría definir lo que es el sexo divorciado de la procreación. (¿Placer mutuo en las áreas genitales? ¿Regalo orgásmico para uno mismo? ¿Union corporal utilizando al menos uno de los órganos reproductivos?) El resultado final es la sexualidad y el sexo se dejan en gran medida indefinidos.

Y entonces, al final de todo esto, tenemos que definir Genero. Es una tarea imposible desde el inicio: El Genero a sido expandido en los últimos años para incluir Asexual, Bisexual, Heterosexual, Homosexual, Pansexual, Intersexual (Me disculpo con aquellos términos que no puedo recordar ahora), y no pareciera detenerse a corto plazo.

Así la frase se convierte en esto: el matrimonio (refiriéndome a la unión erótica de dos personas, sin tener una definición clara para lo erótico, o para la unión) se trata sobre el amor (refiriéndome a /x/), no del genero (refiriendome a /y/).

Así el Cristiano grita por ese Plan Indefinido, los defensores del matrimonio gay gritan por el Amor Indefinido, y todos se largan cabreados, de vuelta a los sitios web, foros y amigos quienes afirmaran nuestras creencias pre-existentes. Si estamos de suerte, alguien nos dará una suavecita y gordita blanca consigna para tirarle a los demás. Oh sí, ¿No lo sabias? Estamos peleando la Gran Guerra del Malvavisco, el encuentro moderno de los caprichos opuestos, y desde que las palabras de uno significan nada en lo absoluto, recurrimos a los insultos para simular que avanzamos algo en la discusión.

Y esto se puede aplicar a cualquier otra cosa que se suele gritar en una manifestación: Igualdad De Derechos Para Todos, El Odio No es Un Valor De La Familia, Pro-Familia, Pro-Matrimonio, El Amor es Ciego al Genero, La Santidad del Matrimonio, Ser Gay es Normal, Besos Diversos --- Estos son días en los que todo el mundo /realmente/ necesitan escuchar lo siguiente:

#+CAPTION: Define tus terminos
http://i.imgur.com/LHWmxfw.png

** 3. Falsas Apariencias

Cómo todo buen propagandista te dirá, una pelea complicada no es nada buena. Para alcanzar nuestra meta, uno debe deshacerse de todos los detalles y presentar el tema en términos de blanco y negro. Así:

[[http://i.imgur.com/gpRo3uH.jpg]] 

Hemos hecho precisamente esto con el debate sobre el matrimonio gay. La guerra a sido en gran parte retratada --- y por favor, díganme si me equivoco --- de la siguiente manera: Las personas gay quieren casarse, los Cristianos no se lo están permitiendo.

La primer parte es falsa. En términos generales, las parejas gay /no/ quieren casarse. Obviamente no tenemos aun disponibles datos universales, pero [[http://www.nationalreview.com/articles/299944/gay-divorcees-charles-c-w-cooke][de lo que sí sabemos ahora]]--- Basado en estudios de Hawaii, Massachusetts, Noruega, Suecia y Holanda --- Las parejas homosexuales están menos dispuestas que las parejas heterosexuales a casarse o entrar en algún equivalente a la unión civil donde este disponible, y son más propensos a divorciarse si lo estuvieran.

Ahora uno podría pensar que seria justo decir, "Sí por supuesto, no todas las parejas gay quieren casarse, pero sí todas las personas gays quieren que el matrimonio gay sea legal," y en gran medida:

http://i.imgur.com/bk8eGCq.png

Pero esto implicaría que el deseo de legalizar el matrimonio gay no es acerca de dejar que las personas hagan lo que profundamente desean, pero sí de normalizar culturalmente un estilo de vida. Y eso es complejo.

Existen hombres y mujeres con atracción por personas del mismo sexo quienes no quieren que el matrimonio gay sea legalizado por ésta razón. Existen hombres y mujeres con atracción por personas del mismo sexo que no quieren el matrimonio gay porque [[http://nogaymarriage.wordpress.com/][es una institución opresiva y patriarcal que debió morir en los 60's]]. En resumen, los hombres y mujeres con atracción por personas del mismo sexo no son esas pobres, descarriadas ovejas, que nosotros amamos tratar como tales. Pero eso es complejo, así que no hablamos de ello.

Similarmente, [[http://www.patheos.com/blogs/badcatholic/2012/05/the-christian-case-for-gay-marriage-the-smackdown.html][no todos los Cristianos]] están en contra del matrimonio gay. Pero no podemos discutir todo esto. "La mayoría de personas con atracción por el mismo sexo quieren, no casarse, pero sí la capacidad para contraer matrimonio, y algunos Cristianos se oponen a esto, por un diverso numero de razones" sencillamente no es un eslogan que podamos usar. ¿Y que diablos haríamos si no tuviéramos eslogans?

** 4. El Hombre Gay cosificado

[[http://www.patheos.com/blogs/badcatholic/2012/05/our-godawful-objectification-of-men-with-same-sex-attraction.html][Yo hablé en profundidad sobre esto en otra entrada, y probablemente sea beneficioso para la conversación si todos lo leyeran]] Pero en breve, todos estamos atacando o defendiendo (con malvaviscos) a un fantasma.

Su nombre es Hombre Gay. Si nos ponemos de pie para defender al Hombre Gay, no lo podemos hacer sin defender la impresión del Hombre Gay que Hollywood nos a marcado en la mente --- es ese lindo, excéntrico, mejor-amigo-de-la-protagonista, espectacular consejero sobre las citas y las compras además de ser en lo absoluto emocionalmente seguro. En otras palabras, nos ponemos de pie para defender algo que más o menos se asemeja a una mascota.

[[http://img2.virgula.uol.com.br/2012/02/17/312557-630x495.jpg]]

Defendemos al Mejor Amigo Gay, como si un hombre con atracción por el mismo sexo fuera cualquier cosa menos un Hombre, creación única, resplandeciente, dotado por Dios con el dominio sobre la Tierra, destinado al amor infinito, exaltado por encima de los ángeles, no solamente igual al hombre heterosexual, sino que también es un hombre que mete la pata tanto como él.

Del mismo modo, si atacamos al Hombre Gay, pareciera que no podemos atacarlo sin el gran cartel de "Pecador" por encima de su cabeza. Es fácil atacar a un hombre por un pecado que nosotros no estamos tentados a cometer, y así lo hacemos con toda la audacia que podríamos usar para decirle a un hombre: "Su deseo de fumar metanfetaminas es desordenado". (oye, ¡buena forma de hacerse notar recto y blanco cristiano!) Todo esto como si el pecador con atracción por el mismo sexo fuera cualquier cosa menos un hombre, no mejor ni peor pecador que nosotros mismos, salvando nuestras almas por el peso de su cruz.

Pero el problema va más aya de cualquier cosificación. La verdadera bofetada en la cara de los hombres y mujeres con atracción por el mismo sexo es éste mismo titulo: Gay.

Gay como la esencia definitoria de nuestro ser. Como he dicho antes, la irrepetible identidad del hombre no esta definido por el lugar donde quiere poner sus genitales, y nunca lo será. No es de extrañar que cualquier disidencia contra los preceptos del Matrimonio Gay choquen de frente con los gritos de "¡Odio!"

Si cosificamos a un hombre, reduciéndolo hasta la palabra Gay, entonces un rechazo de los actos gays se convierte en un rechazo contra /él/. No es de extrañar que podamos hacer nada más que gritar:

#+BEGIN_HTML
<iframe width="420" height="315" src="https://www.youtube-nocookie.com/embed/AMTE8qnJ7h8?rel=0" frameborder="0" allowfullscreen></iframe>
#+END_HTML

** ¿Qué hacer?

Aquí sólo puedo hablar con el Católico --- No tengo ni la menor idea que va hacer el resto del mundo. De hecho, todos los demás, sientan libertad de no continuar la lectura. Si esta en desacuerdo conmigo sobre el asunto del matrimonio gay, no haga nada. Es obvio que las abstracciones están en favor de legalizar el matrimonio gay. Solamente sigan así, hagan todo blanco y negro, odio vs. amor, intolerancia vs. tolerancia, y quizás todos vayamos a aceptar lo erróneo de nuestros caminos.

Pero los Católicos están en una remarcable posición dentro de este debate. El matrimonio es un sacramento, un sacramento que siempre será entre un hombre y una mujer. La Iglesia Católica nunca reconocerá los matrimonios judiciales del siglo 21, ya sea de hombres y mujeres con parejas del mismo sexo o entre parejas heterosexuales. Así, el católico no esta en la posición de excluir a determinado grupo --- el sacramento sigue siendo el sacramento y todo lo demás es falso.

Cuando entonces participamos en la Guerra Malvavisco, puede ser /sólo/ por amor a la persona humana. ¿Qué más tenemos en juego? El Sacramento del Santo Matrimonio no cambiara. ¿La cultura? Sí, la cultura puede que se devalué ¿Pero que es una cultura sino una comunidad de personas humanas únicas?

Parece que sólo debemos molestarnos en hablar si le estamos hablando a la persona humana. Esto quiere decir, en primer lugar, quemar nuestros eslogans. Un eslogan siempre significa venderse a una idea abstracta en lugar de que nos importe el problema. "Yo Apoyo el Matrimonio Tradicional," "Matrimonio = Un Hombre Una Mujer," "Es Adán y Eva no Adán y Esteban" (Para ser justos, nunca he visto esto en la vida real) "El Plan de Dios para el Matrimonio...." No es que estas cosas no sean ciertas, es que estas cosas no vienen de ti, sino de la gran abstracción que son Las Personas Que Apoyan los Valores Tradicionales.

Debemos ver las Cosas por primera vez, como niños pequeños. Debemos ver el matrimonio, el hombre, la mujer, y el amor como Dios los ve a ellos, y expresarnos auténticamente como reacción a estas Verdades. En este determinado sentido, debemos convertirnos en poetas, expresando la Belleza de la Verdad. Si tu honesta reacción hacia el efecto del Matrimonio Gay es "Apoyo el Plan de Dios para el Matrimonio", entonces esta bien. Has un cartel. De lo contrario ponga algo de meditación y oración en ella y /hable./

Entonces debemos definir nuestros términos. Por suerte, la Iglesia Católica invento la mitad de las malditas palabras que usamos en primer lugar. Cuando hablamos de Amor, debemos referirnos a la Caridad. Tenemos que referirnos "Al habito divinamente infundado, que inclina la voluntad humana a amar a Dios por encima de todas las cosas, y al hombre por amor a Dios" y así desear a todos los hombres el bien ultimo, el cual es Dios. Debemos ser catequizados. (Aviso practico: Sí tienes una frase que usas mucho, busca cada uno de los sustantivos que están en el Catecismo, o búscalos [[http://www.newadvent.org/cathen/][aquí]]. Si el matrimonio es un sacramento que une al hombre y a la mujer en Dios, ¿Qué es un sacramento? ¿Qué es un Hombre? ¿Qué es una Mujer? ¿Qué es Dios? y a partir de aquí, ¿Qué es la sexualidad humana?)

Por sobre todas las cosas, debemos amar. Esta no es la parte en donde yo digo, "¡Yo amo las personas gays!, ¡sólo mira todos los amigos gay que tengo!" Porque en pocas palabras, Soy malo amando, punto. Amar es morir por los demás. Amar a nuestros enemigos es morir por nuestros enemigos. Esta no es la frase que deberíamos usar cuando hablamos con los demás del entendimiento Católico sobre el Matrimonio Gay. Esta es la mentalidad por la cual nuestro ser debería luchar. "Porque yo moriría por ti, te lo digo..."

Solamente cuando dejemos de luchar por los Valores Tradicionales, y comencemos a luchar por amor a esa única, persona humana --- solamente entonces cambiaremos algo.

Siempre vuestro.

#+BEGIN_QUOTE
  Una malisima traducción de Shackra Sislock aka Sweet, Fuente original: [[http://www.patheos.com/blogs/badcatholic/2012/05/4-ways-the-gay-marriage-debate-has-been-rigged.html][BadCatholic --- 4 Ways The Gay Marriage Debate Has Been Rigged]].
#+END_QUOTE

*[AMS]: Atracción por el mismo sexo
