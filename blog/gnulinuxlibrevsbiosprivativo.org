#+TITLE: Si mi pie nunca puede curarse, los tratamientos para mi ojo son pérdidas de tiempo o por qué debes usar GNU/Linux-libre aunque tengas un BIOS privativo
#+AUTHOR: Jorge Araya Navarro
#+EMAIL: elcorreo@deshackra.com
#+Date: 2012-03-09
#+LANGUAGE: en
#+URI: /blog/%y/%m/%d/%t/
#+OPTIONS: H:3 num:nil toc:nil \n:nil ::t |:t ^:nil -:nil f:t *:t <:t
#+TAGS: software libre
#+KEYWORDS: software libre

Ayer por la noche inicio las "Cyber Tertulias" en [[irc://irc.freenode.net/sl-centroamerica][#sl-centroamerica]], los temas tratados fueron sobre las políticas de estado para el software libre en Latinoamericana y sobre el soporte a los nuevos usuarios de GNU/Linux. Quería participar en esa primer cyber tertulia porque traía entre manos una propuesta (aunque no quedo muy bien propuesto en la charla) sobre como evitar que las personas se pasaran de GNU/Linux-libre de vuelta a Windows ~~poniendole un revolver en la cabeza a todos los usuarios cada noche antes de que se metan a la cama, glol~~, que fue una idea sencilla que vino a mi cabeza en momentos de necesidad y de innovación en cuanto a comercialización del software libre, que espero, hablarles de ella en detalle más adelante.

Pero no vengo hoy a hablarles sobre lo discutido en la /cyber tertulia/, sino más bien, de un tema que nunca acaba. El tema de la "inclusión". Como bien dice la palabra, la inclusión es... incluir. Y en el contexto del movimiento de software libre, aparentemente es, instalar distribuciones de GNU/Linux con partes privativas para que las personas puedan liberarse de Windows, */WTF?/*. Creo que valdría aclarar algunas cosas con respecto al movimiento de software ¿no? para eso, vamos a mirar lo siguiente que es raíz de esté movimiento.

#+BEGIN_QUOTE
  El software libre es una cuestión de la libertad de los usuarios de ejecutar, copiar, distribuir, estudiar,   cambiar y mejorar el software. Más precisamente, significa que los usuarios de programas tienen las cuatro   libertades esenciales.

  -  La libertad de ejecutar el programa, para cualquier propósito (libertad 0).
  -  La libertad de estudiar cómo trabaja el programa, y cambiarlo para que haga lo que usted quiera (libertad 1). El acceso al código fuente es una condición necesaria para ello.
  -  La libertad de redistribuir copias para que pueda ayudar al prójimo (libertad 2).
  -  La libertad de distribuir copias de sus versiones modificadas a terceros (libertad 3). Si lo hace, puede dar a toda la comunidad una oportunidad de beneficiarse de sus cambios. El acceso al código fuente es una condición necesaria para ello.

  Un programa es software libre si los usuarios tienen todas esas libertades. Entonces, debería ser libre de   redistribuir copias, tanto con o sin modificaciones, ya sea gratis o cobrando una tarifa por distribución, [[http://www.gnu.org/philosophy/free-sw.es.html#exportcontrol][a   cualquiera en cualquier parte]]. El ser libre de hacer estas cosas significa, entre otras cosas, que no tiene   que pedir o pagar el permiso.

  También debería tener la libertad de hacer modificaciones y usarlas en privado, en su propio trabajo u obra,   sin siquiera mencionar que existen. Si publica sus cambios, no debería estar obligado a notificarlo a   alguien en particular, o de alguna forma en particular.
#+END_QUOTE

Lo anterior es la [[http://www.gnu.org/philosophy/free-sw.es.html][definición del software libre mantenida por la Free Software Foundation]]. Si un software no calza en la descripción de arriba, no es software libre, por lo tanto no es parte del movimiento de software libre y por lo tanto, instalar ese software en cualquier maquina no es bueno.

Me encantaría enumerar aquí las razones por las cuales, el software privativo no debe usarse en una computadora, pero no quiero alargar el /post/ y de seguro hablare sobre el tema más adelante (o subiré mi /podcast/ que hice en [[http://www.radiognu.org][RadioGNU]] hace un tiempo donde hablo precisamente del tema).

Pues bien, a pesar de que uno que otro software no sea libre, algunos insisten en que hacen lo correcto al "incluir" a otras personas en el movimiento instalando /X/ software privativo porque así le funciona mejor la computadora y se reducen gastos, haciendo caso, como quien dice, de la "realidad actual" y no yendo contra corriente. Para aquellos que hasta ahora siguen leyendo pero no se ubican acerca de la gravedad del asunto, dado que el movimiento de software libre no es solo tecnológico sino también social, podemos usar la siguiente analogia que suena así:

#+BEGIN_QUOTE
Tratar de mejorar a la sociedad (en materia tecnológica que es la base de sí misma, por cierto) repartiendo software privativo es similar a tratar de tener una sociedad sana repartiendo cigarrillos o drogas.
#+END_QUOTE

Así de loco suena, al menos para muchas personas que aprecian su libertad, el oír y ver como en los Festivales de Instalación de Software Libre (FliSol) una persona se lleva su maquina con un flamante /Ubunchu/ o /Mebian/ (y si hay algún valiente, quizás /Arcolinux/). Y eso, de verdad no ayuda (¡¡y menos ayuda no decirle a las personas que están portando software privativo en sus computadoras!!). ¡/Pero es que solo es un 1% de software privativo para que le funcione la wifi al muchacho y el resto si es software libre!/ ---me dirían--- ¿¡Y qué!? *Si le sumas a cinco bolsas de harina de buena calidad una bolsa de harina de mala calidad (con hongos y toda la vaina!), obtendrás 6 bolsas de harina de mala calidad (con hongos y toda la vaina!)* ¿Porqué insistes en ayudar a que tu prójimo se haga daño?.

En la /cyber tertulia/ fui muy enfático con la persona que discutía, */si la solución más practica es instalar software privativo, mejor lo dejamos ahí/*. Pero, eso no sucede en la mayoría de los casos por miedo a caer mal a las personas que nos viene pidiendo ayuda o a las personas que asisten a los *FliSoles*. Por supuesto que también es injusto no avisar con antelación a los que asisten que, si su computadora requiere un /driver/ privativo, lo mejor [[http://h-node.com][es conseguir esa parte de hardware que si funciona con un driver libre]] y luego, asistir o pedir ayuda en la lista de correo si ya es tarde para presentarse al evento (o pidiendo un instalador en la misma lista, sale mejor).

Si, quizás para algunos las ultimas lineas sonaron más alocadas que la analogia que dí más arriba, pero, es casi inevitable que exprese un "así debería de ser" de mi parte... pero claro, así no sucede en el 99.99% de las veces. *bienvenidos al mundo real*, glol. Claro que /boycottiar/ a las empresas de hardware que no respetan tu libertad no es mala idea, siempre le puedes comprar a la competencia porqué si las empresas de software privativo como Microsoft presionan para que su sistema operativo privativo corra en cualquier hardware ¿Porqué nosotros no podemos hacer lo mismo como consumidores? *después de todo nosotros somos los del dinero en el bolsillo, no ellos*.

Algo que siempre me a molestado, como pseudo-activista, es el tener una /BIOS/ privativa. Si uno aboga para que las demás personas cambien su /wifi/, su tarjeta gráfica o su diente azul por uno que funcione con Software Libre ¿Por qué yo no he reemplazado mi placa madre entonces? *y eso fue un excelente golpe a la quijada de parte de la persona con la que discutía en la tertulia*. Bajo ese cuestionamiento, siquiera valdría la pena tener una distribución de GNU/Linux libre corriendo en mi maquina.

*¡Definitivamente, No sera el primero y único que me eche en cara ese problema!.*

Por ello, antes de dormir, le mande un e-mail a Richard Stallman con esas inquietudes. Y la respuesta no es de menos preciar, considero que existe una buena defensa en su argumentación de la que todos los que deseamos una sociedad digital libre podemos agarrarnos para mantener nuestra postura y justificarla:

#+BEGIN_QUOTE
Yo: Pero ¿Vale la pena usar una distribución sin partes privativas cuando nuestra BIOS es privativa?

RMS: Sí, porque cada paso hacia la libertad es importante. *Cada paso por delante es progreso porque te   liberta de un aspecto de opresión y de un opresor*. También *disminuye el tamaño de la tarea* que quede   por hacer, *y su visible disminución anima a gente a intentar otro paso*.
#+END_QUOTE

Y conste, eso no justifica que una persona ayude a otra a instalar Adobe Flash Player o Skype solo para evitar
el "repudio" del usuario hacia GNU/Linux. Entre menos software privativo en el sistema operativo, mucho más
cerca de ser un ser digital libre y no al revés.

#+BEGIN_QUOTE
Yo: También me inquieta el problema de algunas personas que tienen antenas "wifi" y/o tarjetas de video que requieren software privativo para funcionar. Lo ideal seria cambiarlas por otras que funcionen con "drivers" libres pero ¿Éste cambio valdría la pena a pesar de seguir teniendo una BIOS privativa en la computadora?

RMS: Sí por la misma razón. Si llegas al punto donde sólo el BIOS necesita reemplazarse, por fin lo reemplazarás, quizás substituyendo otra computadora que funcione con BIOS libre. Entonces llegas al munco (sic) libre.
#+END_QUOTE

Descargue toda esa incomodidad en RMS, después de todo, el siempre a sido la persona más importante en el movimiento de software libre hasta el día de hoy ;D. Puede que algún día mi portátil deje de funcionar, o que *CoreBoot* este listo para mi portátil, de igual manera, mi próxima compra debería ser apuntando hacia un /BIOS/ que pueda ser libre.

#+BEGIN_QUOTE
Yo: Y si la solución es adquirir una Yeelong entonces ¿Las 9 distribuciones de GNU/Linux libres fueron, en cierta medida, una perdida de tiempo?

RMS: Usar un Yeeloong e instalar gNewSense o Parabola sería una solición completa, pero no es la sola (sic). Hay tipos de PCs que pueden usar CoreBoot, y toda distro libre puede funcionar en ellos. Pero aunque el usuario tiene el BIOS privativo, eliminar lo demás es no obstante un avance. Esa postura es como decir, "Si mi pie nunca puede curarse, los tratamientos para mi ojo son pérdidas de tiempo." Algo tonto, pienso.
#+END_QUOTE

Excelente respuesta de RMS, pensé que con mi planteamiento se la dejaría difícil, pero por lo visto, le fue tan fácil contestar como lo seria para William Lane Craig con la clásica pregunta atea /¿Quien diseño al diseñador?/ :D.

#+BEGIN_QUOTE
  "Si mi pie nunca puede curarse, los tratamientos para mi ojo son pérdidas de tiempo." Algo tonto, pienso.
#+END_QUOTE

¿Debería mandarlo a enmarcar? xD

En fin, eso de quitar Windows pero dejar software privativo en el GNU/Linux de alguien más para que el cambio "no sea muy brusco" es toda una falacia. Si te dices llamar activista del software libre, al menos, actúa como se supone que debe actuar un activista, en caso contrario, [[http://www.gnu.org/philosophy/open-source-misses-the-point.es.html][sos promotor del Código Abierto]]. Ya lo fijo [[http://www.exnovia.com.ar/abc.htm][Fabio Fusaro]]: *SER = DECIR + HACER*.
